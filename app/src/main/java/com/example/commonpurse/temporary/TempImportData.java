package com.example.commonpurse.temporary;

import android.util.Log;

import com.example.commonpurse.customClasses.payments.Payment;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.math.BigDecimal;
import java.util.ArrayList;

public class TempImportData {

    private static final String TAG = TempImportData.class.getSimpleName();

    public static void importOldData(String dbName){
        FirebaseDatabase fdb = FirebaseDatabase.getInstance();
        DatabaseReference luciaDbRef = fdb.getReference().child("Lucia");
        DatabaseReference robDbRef = fdb.getReference().child("Robbert");
        DatabaseReference newDbRef = fdb.getReference().child(dbName).child("Payments");

        ArrayList<Payment> luciaPayments = new ArrayList<>();
        ArrayList<Payment> robbertPayments = new ArrayList<>();
        ArrayList<Payment> newPayments = new ArrayList<>();

        newDbRef.removeValue((databaseError, databaseReference) -> {
            luciaDbRef.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    Payment newPayment = dataSnapshot.getValue(Payment.class);
                    if (!luciaPayments.contains(newPayment)) {
                        luciaPayments.add(newPayment);
                    } else {
                        Log.d("TempImportData", "This payment is already in the list." +
                                "Something must have gone wrong");
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            luciaDbRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (Payment p: luciaPayments){
                        Payment newLuciaPayment = new Payment();
                        Payment newRobbertPayment = new Payment();

                        BigDecimal totalAmount = new BigDecimal(p.getTotalAmount());
                        BigDecimal amount = new BigDecimal(p.getAmount());
                        newLuciaPayment.setAmount(totalAmount.subtract(amount).floatValue());
                        newRobbertPayment.setAmount(amount.floatValue());

                        newLuciaPayment.setApproved(true);
                        newRobbertPayment.setApproved(true);

                        newLuciaPayment.setBasePayment(false);
                        newRobbertPayment.setBasePayment(true);

                        newLuciaPayment.setDescription(p.getDescription());
                        newRobbertPayment.setDescription(p.getDescription());

                        newLuciaPayment.setCurrency(p.getCurrency());
                        newRobbertPayment.setCurrency(p.getCurrency());

                        newLuciaPayment.setmOriginalAmount(p.getmOriginalAmount());
                        newRobbertPayment.setmOriginalAmount(p.getmOriginalAmount());

                        if (p.getmOriginalAmount() != 0){
                            BigDecimal originalAmount = new BigDecimal(p.getmOriginalAmount());
                            BigDecimal totAmount = new BigDecimal(p.getTotalAmount());
                            double exchangeRate = totAmount.divide(originalAmount, 2,
                                    BigDecimal.ROUND_HALF_UP).doubleValue();
                            newLuciaPayment.setExchangeRate(exchangeRate);
                            newRobbertPayment.setExchangeRate(exchangeRate);
                        }

                        newLuciaPayment.setmTime(p.getmTime());
                        newRobbertPayment.setmTime(p.getmTime());

                        newLuciaPayment.setTotalAmount(p.getTotalAmount());
                        newRobbertPayment.setTotalAmount(p.getTotalAmount());

                        newLuciaPayment.setReceiver("LuciaMajerikova1563269056400");
                        newRobbertPayment.setReceiver("WULKC4MY75fIMvZ2vscqsoxjaV23");

                        if (p.getmCreator() != null){
                            if (p.getmCreator().equals("Anonymous") || p.getmCreator().equals("Lucia")) {
                                newLuciaPayment.setmCreator("LuciaMajerikova1563269056400");
                                newRobbertPayment.setmCreator("LuciaMajerikova1563269056400");
                            }  else if (p.getmCreator().equals("Robbert Kluwer")){
                                newLuciaPayment.setmCreator("WULKC4MY75fIMvZ2vscqsoxjaV23");
                                newRobbertPayment.setmCreator("WULKC4MY75fIMvZ2vscqsoxjaV23");
                            }
                        }

                        if (p.getUser() != null){
                            if (p.getUser().equals("Anonymous") || p.getUser().equals("Lucia")){
                                newLuciaPayment.setUser("LuciaMajerikova1563269056400");
                                newRobbertPayment.setUser("LuciaMajerikova1563269056400");
                            } else if (p.getUser().equals("Robbert Kluwer")){
                                newLuciaPayment.setUser("WULKC4MY75fIMvZ2vscqsoxjaV23");
                                newRobbertPayment.setUser("WULKC4MY75fIMvZ2vscqsoxjaV23");
                            }
                        }

                        newPayments.add(newLuciaPayment);
                        newPayments.add(newRobbertPayment);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            robDbRef.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    Payment newPayment = dataSnapshot.getValue(Payment.class);
                    robbertPayments.add(newPayment);
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            robDbRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (Payment p: robbertPayments){
                        Payment newLuciaPayment = new Payment();
                        Payment newRobbertPayment = new Payment();

                        BigDecimal totalAmount = new BigDecimal(p.getTotalAmount());
                        BigDecimal amount = new BigDecimal(p.getAmount());
                        newRobbertPayment.setAmount(totalAmount.subtract(amount).floatValue());
                        newLuciaPayment.setAmount(amount.floatValue());

                        newLuciaPayment.setApproved(true);
                        newRobbertPayment.setApproved(true);

                        newLuciaPayment.setBasePayment(false);
                        newRobbertPayment.setBasePayment(true);

                        newLuciaPayment.setDescription(p.getDescription());
                        newRobbertPayment.setDescription(p.getDescription());

                        newLuciaPayment.setCurrency(p.getCurrency());
                        newRobbertPayment.setCurrency(p.getCurrency());

                        newLuciaPayment.setmOriginalAmount(p.getmOriginalAmount());
                        newRobbertPayment.setmOriginalAmount(p.getmOriginalAmount());

                        if (p.getmOriginalAmount() != 0){
                            BigDecimal originalAmount = new BigDecimal(p.getmOriginalAmount());
                            BigDecimal totAmount = new BigDecimal(p.getTotalAmount());
                            double exchangeRate = totAmount.divide(originalAmount, 2,
                                    BigDecimal.ROUND_HALF_UP).doubleValue();
                            newLuciaPayment.setExchangeRate(exchangeRate);
                            newRobbertPayment.setExchangeRate(exchangeRate);
                        }

                        newLuciaPayment.setmTime(p.getmTime());
                        newRobbertPayment.setmTime(p.getmTime());

                        newLuciaPayment.setTotalAmount(p.getTotalAmount());
                        newRobbertPayment.setTotalAmount(p.getTotalAmount());

                        newLuciaPayment.setReceiver("LuciaMajerikova1563269056400");
                        newRobbertPayment.setReceiver("WULKC4MY75fIMvZ2vscqsoxjaV23");

                        if (p.getmCreator() != null){
                            if (p.getmCreator().equals("Anonymous") || p.getmCreator().equals("Robbert")) {
                                newLuciaPayment.setmCreator("WULKC4MY75fIMvZ2vscqsoxjaV23");
                                newRobbertPayment.setmCreator("WULKC4MY75fIMvZ2vscqsoxjaV23");
                            }  else if (p.getmCreator().equals("Lucia")){
                                newLuciaPayment.setmCreator("LuciaMajerikova1563269056400");
                                newRobbertPayment.setmCreator("LuciaMajerikova1563269056400");
                            }
                        }

                        if (p.getUser() != null){
                            if (p.getUser().equals("Anonymous") || p.getUser().equals("Robbert")){
                                newLuciaPayment.setUser("WULKC4MY75fIMvZ2vscqsoxjaV23");
                                newRobbertPayment.setUser("WULKC4MY75fIMvZ2vscqsoxjaV23");
                            } else if (p.getUser().equals("Lucia")){
                                newLuciaPayment.setUser("LuciaMajerikova1563269056400");
                                newRobbertPayment.setUser("LuciaMajerikova1563269056400");
                            }
                        }

                        newPayments.add(newLuciaPayment);
                        newPayments.add(newRobbertPayment);
                    }

                    for (Payment p: newPayments){
                        newDbRef.push().setValue(p);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        });
    }
}
