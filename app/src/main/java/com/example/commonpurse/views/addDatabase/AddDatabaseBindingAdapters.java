package com.example.commonpurse.views.addDatabase;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import androidx.databinding.BindingAdapter;

import androidx.databinding.InverseBindingAdapter;
import androidx.databinding.InverseBindingListener;

public class AddDatabaseBindingAdapters {

    @BindingAdapter("app:passwordWarningVisibility")
    public static void passwordWarningVisibility(View view, String password){
        view.setVisibility(password.length() > 6 ? View.GONE : View.VISIBLE);
    }

    /**
     * Listener for the Currency spinner in the add Database view.
     * @param view = Spinner
     * @param selectedItemPositionChange = listener for position change of the spinner.
     */
    @BindingAdapter("app:currencyAttrChanged")
    public void setSelectedItemPositionListener(AdapterView view, final InverseBindingListener selectedItemPositionChange) {
        if (selectedItemPositionChange == null) {
            view.setOnItemSelectedListener(null);
        } else {
            view.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectedItemPositionChange.onChange();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    @BindingAdapter("currency")
    public static void setCurrency(AdapterView view, String currency){
        String selectedItem = (String) view.getSelectedItem();
        if (!selectedItem.equals(currency)){
            view.setSelection(view.getSelectedItemPosition());
        }
    }

    @InverseBindingAdapter(attribute = "currency")
    public static String getCurrency(AdapterView view){
        return (String) view.getSelectedItem();
    }

}
