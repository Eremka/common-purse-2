package com.example.commonpurse.views.addDatabase;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.commonpurse.databinding.FragmentAddDatabaseBinding;
import com.example.commonpurse.fragments.viewModels.DatabaseListViewModel;
import com.example.commonpurse.fragments.viewModels.UserAndDatabaseViewModel;
import com.example.commonpurse.helpers.viewHelpers.HideSoftInputKeyboard;
import com.example.commonpurse.interfaces.DrawerLocker;
import com.example.commonpurse.R;

public class AddDatabaseFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private static final String TAG = AddDatabaseFragment.class.getSimpleName();

    private DatabaseListViewModel databaseListViewModel;
    private UserAndDatabaseViewModel userAndDatabaseViewModel;
    private AddDatabaseViewModel addDatabaseViewModel;

    private EditText databaseNameEt, databasePasswordEt;
    private TextView databaseNameWarningTv, passwordLengthWarningTv;
    private Spinner currencySpinner;

    private FragmentAddDatabaseBinding binding;

    private String currency;

    public AddDatabaseFragment() {
        // Required empty public constructor
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_action_save:
                saveDatabase();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.action_save_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);

        // Disabling the DrawerLayout
        ((DrawerLocker)getActivity()).setDrawerEnabled(false);
        getActivity().setTitle(getString(R.string.create_a_group));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_database, container, false);

        databaseListViewModel = ViewModelProviders.of(this).get(DatabaseListViewModel.class);
        userAndDatabaseViewModel = ViewModelProviders.of(this).get(UserAndDatabaseViewModel.class);
        //addDatabaseViewModel = ViewModelProviders.of(this).get(AddDatabaseViewModel.class);

        binding.setViewModel(new AddDatabaseViewModel());
        binding.setLifecycleOwner(this);

        /*
        passwordLengthWarningTv = view.findViewById(R.id.add_database_password_warning_tv);
        databaseNameWarningTv = view.findViewById(R.id.add_database_warning_tv);
        databaseNameEt = view.findViewById(R.id.add_database_name_et);
        databasePasswordEt = view.findViewById(R.id.add_database_password_et);
        currencySpinner = view.findViewById(R.id.add_database_currency_spinner);
        currencySpinner.setOnItemSelectedListener(this);
        databaseNameEt.requestFocus();

        showDatabaseNameWarning(this, databaseNameEt, databaseNameWarningTv);
        showPasswordLengthWarning(databasePasswordEt, passwordLengthWarningTv);

         */

        return binding.getRoot();
    }


    private void saveDatabase(){
        /*
        Databases db = new Databases();
        try {
            db.setName(databaseNameEt.getText().toString().trim());
            db.setPassword(databasePasswordEt.getText().toString().trim());
            db.setNrOfUsers(1);
            db.setCurrency(currency);
            // TODO get the user and set as creator
        } catch (Exception e) {
            Toast.makeText(getActivity(), getResources().getString(R.string.database_requirements_not_met),
                    Toast.LENGTH_SHORT).show();
        }

        // When the warnings are gone, it means that the conditions for saving the database are met.
        // Thus the database can be saved.
        // The database is also saved in local storage and there is a transaction to add the user
        // to the database.
        if (passwordLengthWarningTv.getVisibility() == View.GONE
                && databaseNameWarningTv.getVisibility() == View.GONE){
            databaseListViewModel.saveDatabase(db);
            userAndDatabaseViewModel.saveCurrentDatabase(db);

            ActionAddDatabaseFragmentToAddUserFragment direction = AddDatabaseFragmentDirections.actionAddDatabaseFragmentToAddUserFragment();
            direction.setUserEmail(userAndDatabaseViewModel.getCurrentUserEmail())
                    .setUserFirebaseId(userAndDatabaseViewModel.getCurrentUserId());
            Navigation.findNavController(Objects.requireNonNull(getView())).navigate(direction);
        }

         */

    }

    /*
    private void showDatabaseNameWarning(Fragment context, EditText nameEt, TextView warningTv){
        nameEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                warningTv.setText(getResources().getString(R.string.database_already_exitst));
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                databaseListViewModel.isDatabaseInSystem(context, s.toString()).observe(context, aBoolean -> {
                    if (aBoolean){
                        warningTv.setVisibility(View.VISIBLE);
                    } else {
                        warningTv.setVisibility(View.GONE);
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void showPasswordLengthWarning(EditText passwordEt, TextView warningTv){

        passwordEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                warningTv.setText(getResources().getString(R.string.password_too_short_warning));
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 6){
                    warningTv.setVisibility(View.GONE);
                } else {
                    warningTv.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

     */

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        currency = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // Empty as there is no nothing selected field.
    }


    /**
     * Closing the soft keyboard
     */
    @Override
    public void onPause() {
        HideSoftInputKeyboard.hideKeyboardFrom(getActivity(), getView());
        super.onPause();
    }
}
