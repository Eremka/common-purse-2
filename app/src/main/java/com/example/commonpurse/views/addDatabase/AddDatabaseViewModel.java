package com.example.commonpurse.views.addDatabase;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;

import com.example.commonpurse.customClasses.databases.Databases;
import com.example.commonpurse.fragments.repositories.DatabasesRepository;

public class AddDatabaseViewModel extends BaseObservable {

    private Databases database = new Databases();
    private DatabasesRepository databasesRepository;

    public AddDatabaseViewModel() {
        databasesRepository = DatabasesRepository.getInstance();
    }

    @Bindable
    public String getDatabaseName(){
        return database.getName();
    }

    public void setDatabaseName(String name){
        this.database.setName(name);
        notifyPropertyChanged(BR.databaseName);
    }

    @Bindable
    public String getDatabasePassword(){
        return database.getPassword();
    }

    public void setDatabasePassword(String password){
        this.database.setPassword(password);
        notifyPropertyChanged(BR.databasePassword);
    }

    @Bindable
    public String getDatabaseCreator(){
        return this.database.getCreator();
    }

    public void setDatabaseCreator(String creator){
        this.database.setCreator(creator);
        notifyPropertyChanged(BR.databaseCreator);
    }

    @Bindable
    public String getDatabaseCurrency(){
        return this.database.getCurrency();
    }

    public void setDatabaseCurrency(String currency){
        this.database.setCurrency(currency);
        notifyPropertyChanged(BR.databaseCurrency);
    }

    public void saveDatabase(){
        // Temporary nr of Users;
        database.setNrOfUsers(1);
        database.setTimeCreated(System.currentTimeMillis());
        databasesRepository.saveDatabase(database);
    }
}
