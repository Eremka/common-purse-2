package com.example.commonpurse.interfaces;

import com.example.commonpurse.customClasses.databases.Databases;

public interface OnDatabaseClickListener {
    void onDatabaseClick(Databases database);
}
