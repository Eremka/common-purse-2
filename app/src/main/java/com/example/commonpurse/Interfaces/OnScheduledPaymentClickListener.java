package com.example.commonpurse.interfaces;

import com.example.commonpurse.customClasses.payments.ScheduledPayment;

public interface OnScheduledPaymentClickListener {
    void onScheduledPaymentClick(ScheduledPayment scheduledPayment);
}
