package com.example.commonpurse.interfaces;

import com.example.commonpurse.customClasses.payments.Payment;

public interface OnPaymentClickListener {
    void onPaymentClick(Payment payment);
}
