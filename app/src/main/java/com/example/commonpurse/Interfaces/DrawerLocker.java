package com.example.commonpurse.interfaces;

import com.example.commonpurse.customClasses.users.User;

public interface DrawerLocker {
    void setDrawerEnabled(boolean enabled);

    void setUser(User user);
}
