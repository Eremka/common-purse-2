package com.example.commonpurse.interfaces;

import com.example.commonpurse.customClasses.users.User;

public interface OnUserClickListener {
    void onUserClick(User user);
}
