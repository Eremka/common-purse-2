package com.example.commonpurse;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.commonpurse.fragments.viewModels.LoginViewModel;
import com.example.commonpurse.fragments.views.AddUserFragment;
import com.example.commonpurse.fragments.views.OverviewFragment.OverviewFragment;
import com.example.commonpurse.fragments.workManagers.ScheduledPaymentWorker;
import com.example.commonpurse.helpers.constants.Constants;
import com.example.commonpurse.interfaces.DrawerLocker;
import com.example.commonpurse.customClasses.users.User;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import androidx.work.Constraints;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * This is the Initial version!
 */


public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener, DrawerLocker {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String PERIODIC_WORK_SCHEDULED_PAYMENT = "PeriodicWorkScheduledRequest";

    private DrawerLayout drawerLayout;
    private LoginViewModel loginViewModel;

    private TextView nameTv, emailTv;
    private CircleImageView profileImage;

    private NavController navController;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.activity_main_toolbar);
        setSupportActionBar(toolbar);

        // Setting up the nav controller
        navController = Navigation.findNavController(this, R.id.activity_main_nav_host_fragment);

        drawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navView = findViewById(R.id.activity_main_navigation_view);
        navView.setNavigationItemSelectedListener(this);

        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        profileImage = navView.getHeaderView(0).findViewById(R.id.drawer_menu_profile);
        nameTv = navView.getHeaderView(0).findViewById(R.id.drawer_menu_name_tv);
        emailTv = navView.getHeaderView(0).findViewById(R.id.drawer_menu_email_tv);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
    }

    /**
     * On back pressed handles the Fragment transitions to the previous fragment. It has to do this
     * manually for the child fragments.
     */
    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            FragmentManager fm = getSupportFragmentManager();
            for (Fragment frag : fm.getFragments()) {
                Log.d(TAG, "onBackPressed visible: " + frag.getClass().getSimpleName() + " " + frag.isVisible());
                if (frag.isVisible()) {
                    FragmentManager childFm = frag.getChildFragmentManager();
                    for (Fragment f : childFm.getFragments()) {
                        if (f.isVisible() && f.getClass().equals(OverviewFragment.class)) {
                            OverviewFragment overviewFragment = (OverviewFragment) f;
                            if (!overviewFragment.goToStart()) {
                                super.onBackPressed();
                            }
                        } else {
                            super.onBackPressed();
                        }
                    }
                }

            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        setupScheduledPayments();
    }

    private void setupScheduledPayments() {
        Constraints constraints = new Constraints.Builder()
                .setRequiresBatteryNotLow(true)
                .setRequiresCharging(false)
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();


        PeriodicWorkRequest periodicWorkRequest = new PeriodicWorkRequest.Builder(ScheduledPaymentWorker.class,
                1, TimeUnit.DAYS)
                .setConstraints(constraints)
                .build();

        WorkManager.getInstance(this).enqueueUniquePeriodicWork(PERIODIC_WORK_SCHEDULED_PAYMENT,
                ExistingPeriodicWorkPolicy.KEEP, periodicWorkRequest);
    }

    /**
     * This is the drawer menu, and the actions when clicking an item
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.drawer_log_out:
                GoogleSignInClient googleSignInClient = loginViewModel.getGoogleSignInClient();
                googleSignInClient.signOut()
                        .addOnCompleteListener(this, task -> {
                            FirebaseAuth.getInstance().signOut();
                            finish();
                        });
                break;
            default:
                NavigationUI.onNavDestinationSelected(item, navController);
        }

        // Navigation is always closed when the button is pressed.
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Request permission result comes from the AddUserFragment. However, the result is always send
     * to the activity. Hence it is handled here.
     *
     * @param requestCode = Unique request code
     * @param permissions = list of permissions requested
     * @param grantResults = list of permissions granted
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "onRequestPermissionsResult requestCode: " + requestCode);
        for (String s : permissions) {
            Log.d(TAG, "onRequestPermissionsResult permission: " + s);
        }
        if (requestCode == Constants.PERMISSION_WRITE_STORAGE) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                    if (fragment.getClass().equals(AddUserFragment.class) && fragment.isVisible()) {
                        AddUserFragment frag = (AddUserFragment) fragment;
                        frag.startPhotoIntent();
                    }
                }
            } else {
                Toast.makeText(this, getResources().getString(R.string.must_grant_permission),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * This function handles whether the drawer menu can be opened or whether it should stay put.
     * @param enabled = whether the drawer is currently closed
     */
    public void setDrawerEnabled(boolean enabled) {
        int lockMode = enabled ? DrawerLayout.LOCK_MODE_UNLOCKED :
                DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
        drawerLayout.setDrawerLockMode(lockMode);
        //toggle.setDrawerIndicatorEnabled(enabled);
    }

    /**
     * Sets the user name, email and profile picture in the drawer menu.
     * @param user = class containing the email, name and profile picture link
     */
    @Override
    public void setUser(User user) {
        try {
            nameTv.setText(user.getName());
            emailTv.setText(user.getEmailAddress());

            if (user.getPhotoUri() != null) {
                Glide.with(this)
                        .load(user.getPhotoUri())
                        .into(profileImage);
            }
        } catch (NullPointerException e) {
            Log.d(TAG, "setUser: " + e.getMessage());
        }
    }
}
