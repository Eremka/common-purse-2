package com.example.commonpurse.Interfaces.Apis;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ExchangeRateApi {

    @GET("api/v7/convert")
    Call<Map<String, Float>> getExchangeRate(@Query("q") String currencies,
                                             @Query("compact") String compact,
                                             @Query("apiKey") String apiKey);
}
