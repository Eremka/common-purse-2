package com.example.commonpurse.Interfaces;

import com.example.commonpurse.customClasses.Users.User;

public interface OnUserClickListener {
    void onUserClick(User user);
}
