package com.example.commonpurse.Interfaces;

import com.example.commonpurse.customClasses.Databases.Databases;

public interface OnDatabaseClickListener {
    void onDatabaseClick(Databases database);
}
