package com.example.commonpurse.Interfaces;

import com.example.commonpurse.customClasses.Payments.Payment;

public interface OnPaymentClickListener {
    void onPaymentClick(Payment payment);
}
