package com.example.commonpurse.Interfaces;

import com.example.commonpurse.customClasses.Users.User;

public interface DrawerLocker {
    void setDrawerEnabled(boolean enabled);

    void setUser(User user);
}
