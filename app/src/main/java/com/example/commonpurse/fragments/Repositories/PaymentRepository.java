package com.example.commonpurse.fragments.repositories;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;

import com.example.commonpurse.fragments.models.Payments.LiveDataAllPaymentsModel;
import com.example.commonpurse.fragments.models.Payments.LiveDataPaymentsWithId;
import com.example.commonpurse.fragments.models.Payments.SaveAndRemovePaymentModel;
import com.example.commonpurse.customClasses.payments.Payment;

import java.util.ArrayList;

public class PaymentRepository {
    private static PaymentRepository paymentRepo;

    public static synchronized PaymentRepository getInstance(){
        if (paymentRepo == null) {
            paymentRepo = new PaymentRepository();
        }

        return paymentRepo;
    }

    public LiveData<ArrayList<Payment>> getAllPayments(String databaseName, String tableName){
        return new LiveDataAllPaymentsModel(databaseName, tableName);
    }

    public LiveData<ArrayList<Payment>> getBasePayments(String databaseName, String tableName, String query){
        // This transformations will retrieve all payments and only return payments where
        // isBasePayment returns true. The basePayments variable is not necessary but here
        // solely for clarity.
        LiveData<ArrayList<Payment>> basePayments = Transformations.map(
                new LiveDataAllPaymentsModel(databaseName, tableName), payment -> {
                    ArrayList<Payment> tempBasePaymentList = new ArrayList<>();
                    for (Payment p: payment){
                        // this will return all payments marked as a base payment.
                        if (p.isBasePayment() && query == null){
                            tempBasePaymentList.add(p);
                        }
                        // This will return all payments marked as a base payment and that contain
                        // the query words in the description.
                        else if (p.isBasePayment() && p.getDescription().contains(query)){
                            tempBasePaymentList.add(p);
                        }
                    }
                    return tempBasePaymentList;
                });

        return basePayments;
    }

    public void savePaymentList(String databaseName, String tableName, ArrayList<Payment> paymentList){
        SaveAndRemovePaymentModel saveAndRemovePaymentModel = SaveAndRemovePaymentModel.getInstance(databaseName, tableName);
        for (Payment p : paymentList) {
            saveAndRemovePaymentModel.savePayment(p);
        }
    }

    public LiveData<ArrayList<Payment>> getPaymentWithId (String databaseName, String tableName, long id){
        return new LiveDataPaymentsWithId(databaseName, tableName, id);
    }

    public void removePayment(String databaseName, String tableName, ArrayList<Payment> paymentList) {
        SaveAndRemovePaymentModel saveAndRemovePaymentModel = SaveAndRemovePaymentModel.getInstance(databaseName, tableName);
        for (Payment p: paymentList){
            saveAndRemovePaymentModel.removePayment(p);
        }
    }
}
