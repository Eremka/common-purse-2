package com.example.commonpurse.fragments.dialogFragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.example.commonpurse.interfaces.PasswordChecker;
import com.example.commonpurse.R;

public class PasswordDialogFragment extends DialogFragment {

    private static final String TAG = PasswordDialogFragment.class.getSimpleName();
    private PasswordChecker passwordChecker;
    private String password;

    private EditText passwordEt;


    public PasswordDialogFragment(PasswordChecker passwordChecker, String password) {
        super();
        this.passwordChecker = passwordChecker;
        this.password = password;
        Log.d(TAG, "PasswordDialogFragment called:");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_password_dialog, null);
        passwordEt = view.findViewById(R.id.fragment_password_et);

        builder.setView(view);
        builder.setTitle(getResources().getString(R.string.enter_your_password));
        builder.setPositiveButton(getResources().getString(R.string.ok), (dialog, which) -> {
            // This needs to be overridden in onResume.
            });
        builder.setNegativeButton(getResources().getString(R.string.cancel), (dialog, which) -> {
            dialog.dismiss();
        });

        return builder.create();
    }

    @Override
    public void onResume() {
        super.onResume();
        final AlertDialog dialog = (AlertDialog) getDialog();
        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveButton.setOnClickListener( (v) -> {
            if (password.equals(passwordEt.getText().toString().trim())) {
                passwordChecker.passwordCorrect();
                dialog.dismiss();
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.incorrect_password),
                        Toast.LENGTH_SHORT).show();
                passwordEt.setText("");
            }
        });
    }
}
