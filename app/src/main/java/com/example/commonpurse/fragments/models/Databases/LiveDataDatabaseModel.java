package com.example.commonpurse.fragments.models.Databases;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;

import com.example.commonpurse.helpers.databaseHelper.FirebaseDatabaseHelper;
import com.example.commonpurse.customClasses.databases.Databases;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;


public class LiveDataDatabaseModel extends LiveData<List<Databases>> {

    private static LiveDataDatabaseModel instance;
    private static String databaseName;

    private ArrayList<Databases> allDatabases = new ArrayList<>();

    private static FirebaseDatabase database;
    private static DatabaseReference databaseReference;

    public static LiveDataDatabaseModel getInstance(String database){
        if (instance == null){
            instance = new LiveDataDatabaseModel();
            databaseName = database;

            setupDatabase();
        }

        return instance;
    }

    private static void setupDatabase(){
        database = FirebaseDatabaseHelper.getFirebaseDatabaseInstance();
        databaseReference = database.getReference().child(databaseName);
        databaseReference.keepSynced(true);
    }

    @Override
    protected void onActive() {
        super.onActive();

        databaseReference.addChildEventListener(childEventListener);
    }

    @Override
    protected void onInactive() {
        super.onInactive();

        // Clearing up the databases when they are not used anymore.
        allDatabases.clear();
        databaseReference.removeEventListener(childEventListener);
    }

    private ChildEventListener childEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            Databases currentDb = dataSnapshot.getValue(Databases.class);
            currentDb.setKey(dataSnapshot.getKey());

            if (!allDatabases.contains(currentDb)){
                allDatabases.add(currentDb);
                setValue(allDatabases);
            }
        }

        @Override
        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            Databases currentDb = dataSnapshot.getValue(Databases.class);
            currentDb.setKey(dataSnapshot.getKey());

            for (Databases db: allDatabases){
                if (db.getKey().equals(currentDb.getKey())){
                    db.updateValues(currentDb);
                    setValue(allDatabases);
                }
            }
        }

        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
            for (Databases db: allDatabases){
                if (db.getKey().equals(dataSnapshot.getKey())){
                    allDatabases.remove(db);
                    setValue(allDatabases);
                }
            }
        }

        @Override
        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };
}
