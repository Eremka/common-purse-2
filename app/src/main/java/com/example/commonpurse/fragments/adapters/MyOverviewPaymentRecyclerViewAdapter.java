package com.example.commonpurse.Fragments.Adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.commonpurse.Helpers.StringFormatters.TwoDecimalFormatter;
import com.example.commonpurse.R;
import com.example.commonpurse.customClasses.Payments.Payment;

import java.util.ArrayList;

public class MyOverviewPaymentRecyclerViewAdapter extends RecyclerView.Adapter<MyOverviewPaymentRecyclerViewAdapter.ViewHolder> {

    private static final String TAG = MyOverviewPaymentRecyclerViewAdapter.class.getSimpleName();

    private ArrayList<Payment> paymentList = new ArrayList<>();
    private String currency;

    public MyOverviewPaymentRecyclerViewAdapter(String currency) {
        this.currency = currency;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_overview_user_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Payment currentPayment = paymentList.get(position);

        if (currentPayment.getmCreator() == null){
            Log.d(TAG, "onBindViewHolder: There is no creator for this payment");
            // do nothing
        } else if (currentPayment.getmCreator().equals(currentPayment.getUser())){
            String descriptionText = holder.mView.getContext().getResources().getString(R.string.you_owe)
                    + " "
                    + currentPayment.getReceiver()
                    + ":";
            holder.descriptionTv.setText(descriptionText);
            holder.descriptionTv.setTextColor(holder.mView.getContext().getResources().getColor(R.color.red));
            holder.amountTv.setTextColor(holder.descriptionTv.getCurrentTextColor());
        } else if (currentPayment.getmCreator().equals(currentPayment.getReceiver())){
            String descriptionText = holder.mView.getContext().getResources().getString(R.string.you_are_owed)
                    + " "
                    + currentPayment.getUser()
                    + ":";
            holder.descriptionTv.setText(descriptionText);
            holder.descriptionTv.setTextColor(holder.mView.getContext().getResources().getColor(R.color.green));
            holder.amountTv.setTextColor(holder.descriptionTv.getCurrentTextColor());
        }

        holder.amountTv.setText(TwoDecimalFormatter.formatAmount(currency, currentPayment.getAmount()));
    }

    @Override
    public int getItemCount() {
        return paymentList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView descriptionTv, amountTv;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            descriptionTv = view.findViewById(R.id.overview_list_item_name_tv);
            amountTv = view.findViewById(R.id.overview_list_item_amount_tv);
        }
    }

    public void setData(ArrayList<Payment> paymentList){
        this.paymentList = paymentList;
        notifyDataSetChanged();
    }
}
