package com.example.commonpurse.Fragments.ViewModels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import com.example.commonpurse.Fragments.Repositories.DatabasesRepository;
import com.example.commonpurse.Fragments.Repositories.UserAndDatabaseLocalRepo;
import com.example.commonpurse.Fragments.Repositories.UsersRepository;
import com.example.commonpurse.R;
import com.example.commonpurse.customClasses.Databases.Databases;
import com.example.commonpurse.customClasses.Users.User;

import java.util.List;

public class DatabaseListViewModel extends AndroidViewModel {

    private static final String TAG = DatabaseListViewModel.class.getSimpleName();

    private DatabasesRepository databasesRepository;
    private UsersRepository usersRepository;
    private UserAndDatabaseLocalRepo userAndDatabaseLocalRepo;
    private MutableLiveData<Boolean> isInDatabase = new MutableLiveData<>();

    public DatabaseListViewModel(@NonNull Application application) {
        super(application);

        userAndDatabaseLocalRepo = UserAndDatabaseLocalRepo.getInstance();
        databasesRepository = DatabasesRepository.getInstance();
        usersRepository = UsersRepository.getInstance();
    }

    public LiveData<List<Databases>> getAllDatabases(){
        return databasesRepository.getAllDatabases(
                getApplication().getResources().getString(R.string.database_reference_name));
    }

    public MutableLiveData<Boolean> isDatabaseInSystem(Fragment context, String name){
        isInDatabase.setValue(false);

        if (name.length() > 4) {
            getAllDatabases().observe(context, databases -> {
                for (Databases db : databases) {
                    if (db.getName().toLowerCase().equals(name.toLowerCase())) {
                        isInDatabase.setValue(true);
                    }
                }
            });
        }

        return isInDatabase;
    }

    public LiveData<User> getCurrentUserInDatabase(String databaseName, String id){
        return usersRepository.getUserWithId(databaseName,
                getApplication().getResources().getString(R.string.user_reference_name), id);
    }

    public LiveData<User> getCurrentUserInDatabaseWithEmail(){
        String databaseName = userAndDatabaseLocalRepo.getDatabaseName(getApplication());
        String tableName = getApplication().getResources().getString(R.string.user_reference_name);

        return Transformations.map(usersRepository.getUserWithEmail(databaseName,
                tableName, userAndDatabaseLocalRepo.getCurrentUserEmail(getApplication())), user -> {
            if (user != null) {
                user.setId(user.getId() + "," + userAndDatabaseLocalRepo.getCurrentUserID(getApplication()));
                usersRepository.updateUser(databaseName, tableName, user);
            }

            return user;
            });
    }

    public void saveDatabase(Databases db){
        databasesRepository.saveDatabase(db, getApplication().getResources().getString(R.string.database_reference_name));
    }
}
