package com.example.commonpurse.fragments.views;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.commonpurse.fragments.viewModels.UserAndDatabaseViewModel;
import com.example.commonpurse.fragments.viewModels.UserListViewModel;
import com.example.commonpurse.helpers.constants.Constants;
import com.example.commonpurse.helpers.pictureResizer.PhotoResizer;
import com.example.commonpurse.helpers.viewHelpers.HideSoftInputKeyboard;
import com.example.commonpurse.interfaces.DrawerLocker;
import com.example.commonpurse.R;

import java.io.IOException;
import java.util.Objects;

public class AddUserFragment extends Fragment {

    private static final String TAG = AddUserFragment.class.getSimpleName();
    private static final int PICK_IMAGE = 100;

    private EditText firstNameEt, lastNameEt, emailEt;
    private ImageView profileImage;
    private Bitmap photoBitmap;

    private String firebaseUserId;

    private UserListViewModel userListViewModel;
    private UserAndDatabaseViewModel userAndDatabaseViewModel;

    public AddUserFragment() {
        // Required empty public constructor
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_action_save:
                saveUser();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.action_save_menu, menu);

        getActivity().setTitle(getString(R.string.create_new_user));
        super.onCreateOptionsMenu(menu, inflater);
    }

    /**
     * Getting the foto back from the photo intent. The photo is then set as the profile picture
     * and the photo is resized as well.
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == PICK_IMAGE && resultCode == getActivity().RESULT_OK){
            Uri photoUri = data.getData();
            Log.d(TAG, "onActivityResult photo uri: " + photoUri);

            profileImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
            profileImage.setImageURI(photoUri);

            try {
                photoBitmap = PhotoResizer.resizePhoto(
                        MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), photoUri));
            } catch (IOException e){
                Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_user, container, false);
        setHasOptionsMenu(true);

        // Disabling the DrawerLayout
        ((DrawerLocker)getActivity()).setDrawerEnabled(false);

        userListViewModel = ViewModelProviders.of(this).get(UserListViewModel.class);
        userAndDatabaseViewModel = ViewModelProviders.of(this).get(UserAndDatabaseViewModel.class);

        firstNameEt = view.findViewById(R.id.add_user_name_et);
        lastNameEt = view.findViewById(R.id.add_user_last_name_et);
        emailEt = view.findViewById(R.id.add_user_email_et);
        profileImage = view.findViewById(R.id.add_user_picture_view);

        profileImage.setOnClickListener(v -> {
            checkPermissions();
        });

        setFirstUserData();

        return view;
    }

    private void setFirstUserData() {
        if (getArguments() != null) {
            emailEt.setText(AddUserFragmentArgs.fromBundle(getArguments()).getUserEmail());
            firebaseUserId = AddUserFragmentArgs.fromBundle(getArguments()).getUserFirebaseId();
        }
    }


    private void saveUser(){
        long timeCreated = System.currentTimeMillis();
        String firstName = firstNameEt.getText().toString().trim();
        String lastName = lastNameEt.getText().toString().trim();
        String email = emailEt.getText().toString().trim();

        int result = userListViewModel.saveUser(userAndDatabaseViewModel.getCurrentDatabase(), firstName,
                lastName, email, timeCreated, firebaseUserId, photoBitmap);

        if (result != -1){
            Navigation.findNavController(Objects.requireNonNull(getView()))
                    .navigate(R.id.action_addUserFragment_to_overviewFragment);
        }
    }

    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            startPhotoIntent();
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    Constants.PERMISSION_WRITE_STORAGE);
        }
    }

    public void startPhotoIntent() {
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");

        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");

        Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});

        startActivityForResult(chooserIntent, PICK_IMAGE);
    }

    /**
     * Closing the soft keyboard
     */
    @Override
    public void onPause() {
        HideSoftInputKeyboard.hideKeyboardFrom(getActivity(), getView());
        super.onPause();
    }
}
