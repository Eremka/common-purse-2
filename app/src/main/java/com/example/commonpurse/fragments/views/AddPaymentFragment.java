package com.example.commonpurse.fragments.views;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.commonpurse.fragments.adapters.MyAddPaymentRecyclerViewAdapter;
import com.example.commonpurse.fragments.adapters.SpinnerAdapters.UserSpinnerAdapter;
import com.example.commonpurse.fragments.dialogFragments.SchedulePaymentDialogFragment;
import com.example.commonpurse.fragments.viewModels.AddPaymentViewModel;
import com.example.commonpurse.fragments.viewModels.ExchangeRateViewModel;
import com.example.commonpurse.fragments.viewModels.UserAndDatabaseViewModel;
import com.example.commonpurse.helpers.constants.Constants;
import com.example.commonpurse.helpers.stringFormatters.DescriptionText;
import com.example.commonpurse.helpers.stringFormatters.TwoDecimalFormatter;
import com.example.commonpurse.helpers.viewHelpers.HideSoftInputKeyboard;
import com.example.commonpurse.helpers.viewHelpers.SlowOpenAndCloseView;
import com.example.commonpurse.R;
import com.example.commonpurse.customClasses.payments.Payment;
import com.example.commonpurse.customClasses.users.User;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Objects;


public class AddPaymentFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private static final String TAG = AddPaymentFragment.class.getSimpleName();

    private MyAddPaymentRecyclerViewAdapter myAddPaymentRecyclerViewAdapter;
    private ArrayAdapter<CharSequence> currencySpinnerAdapter;

    private UserAndDatabaseViewModel userAndDatabaseViewModel;
    private AddPaymentViewModel addPaymentViewModel;
    private ExchangeRateViewModel exchangeRateViewModel;

    private View rootView;
    private UserSpinnerAdapter userSpinnerAdapter;
    private RecyclerView recyclerView;
    private Spinner currencySpinner, userSpinner;

    private RelativeLayout scheduledWarningRL;
    private ImageButton cancelScheduledPaymentImageButton;
    private ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener;

    private int viewHeight;
    private boolean isScheduled;
    private String selectedCurrency = null;
    private User selectedUser = null;
    private BigDecimal exchangeRate = BigDecimal.ZERO;

    private EditText amountEt, descriptionEt;
    private TextView exchangeRateTv;

    private ArrayList<User> removedUserList = new ArrayList<>();
    private ArrayList<User> userList;
    private ArrayList<Payment> originalPaymentList = new ArrayList<>();

    private long paymentKey = 0;
    private BigDecimal paymentAmount = new BigDecimal(0);

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public AddPaymentFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.add_payment_menu, menu);

        getActivity().setTitle(getString(R.string.create_a_payment));
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_payment_save:
                // Saving the payments
                savePayments();
                break;
            case R.id.add_payment_add_user:
                // To add the removed user back into the list.
                if (removedUserList.size() != 0) {
                    addUserBackDialog();
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.all_users_in_list),
                            Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.add_payment_delete:
                showDeletePaymentDialog();
                break;
            case R.id.add_payment_schedule:
                showScheduleDialog();
                addPaymentViewModel.isScheduledPayment().observe(this, isScheduled -> {
                    this.isScheduled = isScheduled;
                    if (isScheduled){
                        scheduledWarningRL.setVisibility(View.VISIBLE);
                        SlowOpenAndCloseView.openViewGradually(rootView, scheduledWarningRL, viewHeight);
                    }
                });
                break;
            default:
                Toast.makeText(getActivity(),
                        getResources().getString(R.string.something_has_gone_wrong_warning),
                        Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_add_payment, container, false);
        setHasOptionsMenu(true);

        userAndDatabaseViewModel = ViewModelProviders.of(this).get(UserAndDatabaseViewModel.class);
        addPaymentViewModel = ViewModelProviders.of(getActivity()).get(AddPaymentViewModel.class);
        exchangeRateViewModel = ViewModelProviders.of(this).get(ExchangeRateViewModel.class);

        Context context = rootView.getContext();
        recyclerView = rootView.findViewById(R.id.add_payment_user_list);
        amountEt = rootView.findViewById(R.id.add_payment_amount_et);
        descriptionEt = rootView.findViewById(R.id.add_payment_description_et);
        exchangeRateTv = rootView.findViewById(R.id.add_payment_exchange_rate_tv);

        scheduledWarningRL = rootView.findViewById(R.id.add_payment_scheduled_warning_rl);
        cancelScheduledPaymentImageButton = rootView.findViewById(R.id.add_payment_scheduled_warning_cancel_button);
        getViewHeight(scheduledWarningRL);
        cancelScheduledPaymentImageButton.setOnClickListener(v -> {
            SlowOpenAndCloseView.closeViewGradually(rootView, scheduledWarningRL, viewHeight);
            addPaymentViewModel.setSchedulePayment(false);
        });

        currencySpinnerAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.array_currencies, android.R.layout.simple_spinner_dropdown_item);
        currencySpinner = rootView.findViewById(R.id.add_payment_currency_spinner);
        currencySpinner.setAdapter(currencySpinnerAdapter);
        currencySpinner.setSelection(currencySpinnerAdapter.getPosition(userAndDatabaseViewModel.getCurrentCurrency()));
        currencySpinner.setOnItemSelectedListener(this);

        userSpinner = rootView.findViewById(R.id.add_payment_user_spinner);
        userSpinnerAdapter = new UserSpinnerAdapter(getActivity(), new ArrayList<>());
        userSpinner.setOnItemSelectedListener(this);
        userSpinner.setAdapter(userSpinnerAdapter);

        myAddPaymentRecyclerViewAdapter = new MyAddPaymentRecyclerViewAdapter();
        myAddPaymentRecyclerViewAdapter.setCurrency(userAndDatabaseViewModel.getCurrentCurrency());
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(myAddPaymentRecyclerViewAdapter);
        setItemTouchHelper();

        amountEt.addTextChangedListener(amountTextWatcher);

        getSelectedPaymentId();
        getData();
        setSpinnerData();

        return rootView;
    }

    // This is the on item selected listener for the currency and the user spinners.
    // It handles the events for both spinners.
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.add_payment_currency_spinner:
                selectedCurrency = parent.getItemAtPosition(position).toString().trim();
                if (!selectedCurrency.equals(userAndDatabaseViewModel.getCurrentCurrency())) {
                    getExchangeRate();
                }
                break;
            case R.id.add_payment_user_spinner:
                selectedUser = (User) parent.getItemAtPosition(position);
                break;
            default:
                Toast.makeText(getActivity(), getResources().getString(R.string.something_has_gone_wrong_warning),
                        Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // There is not field for nothing, and thus this method is empty;
    }

    /**
     * Getting the payment Id from the selected payment in the PaymentListFragment. If getArguments
     * is null, the user did not select a previous payment and a new one will need to be created.
     */
    private void getSelectedPaymentId() {
        if (getArguments() != null){
            paymentKey = AddPaymentFragmentArgs.fromBundle(getArguments()).getPaymentId();
        }
    }

    /**
     * This function retrieves the Users from the user List view model. It then passes this
     * data on to the adapter. The adapter than recalculates the amounts for each user.
     * <p>
     * If paymentKey == 0, it means that there is a new Payment being created. Otherwise an existing
     * payment is updated or removed.
     */
    private void getData() {
        addPaymentViewModel.getAllUsers(userAndDatabaseViewModel.getCurrentDatabase()).observe(
                this, users -> {
                    userList = users;
                    if (paymentKey != 0) {
                        addPaymentViewModel.getPaymentsWithId(userAndDatabaseViewModel.getCurrentDatabase()
                                , paymentKey).observe(this, payments -> {
                            originalPaymentList.clear();
                            originalPaymentList = payments;

                            setOriginalPaymentData(payments);
                            removedUserList.clear();
                            removedUserList.addAll(addPaymentViewModel.getRemovedUsers(users, payments));
                            setData(paymentAmount, userList, removedUserList, originalPaymentList);
                        });
                    } else {
                        setData(paymentAmount, userList, removedUserList, originalPaymentList);
                    }
                }
        );

    }

    private void setData(BigDecimal paymentAmount, ArrayList<User> userList,
                         ArrayList<User> removedUserList, ArrayList<Payment> originalPaymentList) {
        myAddPaymentRecyclerViewAdapter.setData(addPaymentViewModel.prepareUsers(paymentAmount,
                userList, removedUserList, originalPaymentList));
    }

    private void setOriginalPaymentData(ArrayList<Payment> payments) {
        Payment examplePayment = payments.get(0);

        try {
            if (examplePayment.getmOriginalAmount() == 0) {
                amountEt.setText(TwoDecimalFormatter.formatAmount(examplePayment.getTotalAmount()));
            } else {
                amountEt.setText(TwoDecimalFormatter.formatAmount(examplePayment.getmOriginalAmount()));
            }

            currencySpinner.setSelection(currencySpinnerAdapter.getPosition(examplePayment.getCurrency()));
            userSpinner.setSelection(userSpinnerAdapter.getPositionFromName(examplePayment.getUser()));
            descriptionEt.setText(examplePayment.getDescription());
        } catch (NullPointerException e) {
            Log.d(TAG, "setOriginalPaymentData error: " + e.getMessage());
        }
    }

    /**
     * This function sets the data in the top spinner. The data is all the active users in the
     * database. It then loads the current user and sets the spinner to this value.
     */
    private void setSpinnerData() {
        addPaymentViewModel.getAllUsers(userAndDatabaseViewModel.getCurrentDatabase()).observe(
                this, users -> {
                    Log.d(TAG, "setSpinnerData results size: " + users.size());
                    // updating the user spinner data
                    userSpinnerAdapter.clear();
                    userSpinnerAdapter.addAll(users);

                    addPaymentViewModel.getUserWithId(userAndDatabaseViewModel.getCurrentDatabase(),
                            userAndDatabaseViewModel.getCurrentUserId()).observe(this, user -> {
                        userSpinner.setSelection(userSpinnerAdapter.getPositionFromName(user.getName()));
                        Log.d(TAG, "setSpinnerData: " + userSpinner.getSelectedItemPosition());
                    });
                }
        );
    }

    // This method is used to get the exchange rate from the api. It then sets this value in the
    // exchange rate variable. If the value is 0, something has gone wrong.
    private void getExchangeRate() {
        Log.d(TAG, "onItemSelected getting currency: ");
        exchangeRateViewModel.getExchangeRate(selectedCurrency,
                userAndDatabaseViewModel.getCurrentCurrency()).observe(this,
                aFloat -> {
                    exchangeRate = new BigDecimal(aFloat);

                    if (aFloat == 0f) {
                        // Show warning that something had gone wrong with getting the
                        // exchange rate
                        exchangeRateTv.setVisibility(View.GONE);
                        Toast.makeText(getActivity(),
                                getResources().getString(R.string.get_exchange_rate_error),
                                Toast.LENGTH_SHORT).show();
                    } else if (aFloat == 1f) {
                        // If the exchange rate is 1, the selected currency is the base currency.
                        exchangeRateTv.setVisibility(View.GONE);
                    } else {
                        // Setting the exchange rate Text View.
                        exchangeRateTv.setText(DescriptionText.getExchangeRateText(
                                userAndDatabaseViewModel.getCurrentCurrency(), selectedCurrency, aFloat));
                        exchangeRateTv.setVisibility(View.VISIBLE);
                    }
                });

    }

    // This function calculates the exact payment that needs to be saved in the database.
    // It then saves it in the database after recalculation, or throws a warning and does nothing else.
    private void savePayments() {
        // Setting the amount to 0 if no amount has been entered
        if (amountEt.getText().length() == 0) {
            amountEt.setText(String.valueOf(0));
        }

        // Getting the payments entered in the recycler view and rounding them off.
        ArrayList<User> returnedList = addPaymentViewModel.calculatePaymentsBeforeSave(
                myAddPaymentRecyclerViewAdapter.getData(), paymentAmount, exchangeRate);

        long time = originalPaymentList.size() == 0 ? System.currentTimeMillis() : originalPaymentList.get(0).getmTime();

        // Returned List == null when the payment is not in balance. This means that the combined
        // amount of the entries in the recycler view do not match the total amount entered
        // in the amount field.
        if (returnedList != null) {
            ArrayList<Payment> paymentList = addPaymentViewModel.createPaymentList(selectedUser,
                    paymentAmount, selectedCurrency, descriptionEt.getText().toString().trim(),
                    exchangeRate, time, returnedList);

            // Saving the payments.
            addPaymentViewModel.savePayments(paymentList, originalPaymentList,
                    userAndDatabaseViewModel.getCurrentDatabase(), isScheduled, selectedUser);

            // Transferring back to the payment list.
            Navigation.findNavController(getView()).navigate(R.id.action_addPaymentFragment_to_paymentListFragment);
        } else {
            Log.d(TAG, "savePayments payment amount: " + paymentAmount);
            Toast.makeText(getActivity(), addPaymentViewModel.createWarningString(
                    myAddPaymentRecyclerViewAdapter.getData(), paymentAmount, selectedCurrency),
                    Toast.LENGTH_SHORT).show();
        }
    }

    // This function creates a dialog that can restore the removed users from the list.
    private void addUserBackDialog() {
        ArrayList<User> returnedUserList = new ArrayList<>();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getActivity().getResources().getString(R.string.add_user));
        String[] userArray = new String[removedUserList.size()];

        // Setting data in the userarray
        for (int i = 0; i < removedUserList.size(); i++) {
            userArray[i] = removedUserList.get(i).getName();
        }

        builder.setMultiChoiceItems(userArray, null, ((dialog, which, isChecked) -> {
            if (isChecked) {
                for (User u : removedUserList) {
                    if (u.getName().equals(userArray[which])) {
                        returnedUserList.add(u);
                        Log.d(TAG, "addUserBackDialog returned user: " + u.toString());
                    }
                }
            }
        }));
        builder.setPositiveButton("OK", (dialog, which) -> {
            removedUserList.removeAll(returnedUserList);
            setData(paymentAmount, userList, removedUserList, null);
            dialog.dismiss();
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> {
            returnedUserList.clear();
            dialog.dismiss();
        });
        builder.show();
    }

    // This text watcher checks for changes in the amount and then sets the amounts on the list
    // of users that this amount was paid for.
    private TextWatcher amountTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            s = s.length() == 0 ? String.valueOf(0) : s;

            paymentAmount = new BigDecimal(s.toString());
            Log.d(TAG, "onTextChanged paymentAmount: " + paymentAmount.toString());
            setData(paymentAmount, userList, removedUserList, null);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    // This function sets an item touch helper on the list of users. This will enable the user
    // to remove users from the list.
    private void setItemTouchHelper() {
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                if (direction == ItemTouchHelper.RIGHT) {
                    if (myAddPaymentRecyclerViewAdapter.getData().size() > 1) {
                        removedUserList.add(myAddPaymentRecyclerViewAdapter.removeUser(viewHolder.getAdapterPosition()));
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.no_user_selected), Toast.LENGTH_SHORT).show();
                    }

                    setData(paymentAmount, userList, removedUserList, null);
                }
            }
        }).attachToRecyclerView(recyclerView);
    }

    private void showDeletePaymentDialog() {
        new AlertDialog.Builder(Objects.requireNonNull(getActivity()))
                .setTitle(getResources().getString(R.string.delete_entry))
                .setMessage(getResources().getString(R.string.sure_to_delete))
                .setPositiveButton("YES", (dialog, which) -> {
                    deletePayment();
                })
                .setNegativeButton("NO", ((dialog, which) -> {
                    dialog.dismiss();
                }))
                .show();
    }

    private void showScheduleDialog() {
        SchedulePaymentDialogFragment dialog = new SchedulePaymentDialogFragment();
        dialog.show(Objects.requireNonNull(getFragmentManager()), Constants.SCHEDULE_DIALOG);
    }

    private void deletePayment() {
        addPaymentViewModel.deletePayment(userAndDatabaseViewModel.getCurrentDatabase(), originalPaymentList);
        Objects.requireNonNull(getActivity()).onBackPressed();
    }

    private void getViewHeight(View view){
        ViewTreeObserver observer = view.getViewTreeObserver();
        onGlobalLayoutListener = () -> {
            viewHeight = view.getHeight();

            view.setVisibility(View.GONE);
            view.getViewTreeObserver().removeOnGlobalLayoutListener(onGlobalLayoutListener);
        };

        observer.addOnGlobalLayoutListener(onGlobalLayoutListener);
    }

    /**
     * Closing the soft keyboard.
     */
    @Override
    public void onPause() {
        HideSoftInputKeyboard.hideKeyboardFrom(getActivity(), getView());
        super.onPause();
    }
}
