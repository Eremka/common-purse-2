package com.example.commonpurse.fragments.viewModels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.commonpurse.fragments.repositories.ExchangeRateRepository;
import com.example.commonpurse.R;

public class ExchangeRateViewModel extends AndroidViewModel {

    private static final String TAG = ExchangeRateViewModel.class.getSimpleName();
    private ExchangeRateRepository exchangeRateRepository = ExchangeRateRepository.getInstance();

    public ExchangeRateViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<Float> getExchangeRate(String baseCurrency, String convertedCurrency){
        String baseUrl = getApplication().getResources().getString(R.string.currency_converter_base_url);
        String userKey = getApplication().getResources().getString(R.string.currency_converter_key);

        return exchangeRateRepository.getExchangeRate(baseUrl, baseCurrency, convertedCurrency, userKey);
    }

}
