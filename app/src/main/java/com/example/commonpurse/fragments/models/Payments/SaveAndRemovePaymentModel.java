package com.example.commonpurse.fragments.models.Payments;

import androidx.annotation.NonNull;

import com.example.commonpurse.helpers.databaseHelper.FirebaseDatabaseHelper;
import com.example.commonpurse.customClasses.payments.Payment;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SaveAndRemovePaymentModel {

    private static SaveAndRemovePaymentModel instance;
    private static DatabaseReference databaseReference;

    public static SaveAndRemovePaymentModel getInstance(@NonNull String databaseName, @NonNull String tableName){
        if (instance == null){
            instance = new SaveAndRemovePaymentModel();
        }

        setupDatabase(databaseName, tableName);
        return instance;
    }

    private static void setupDatabase(String databaseName, String tableName){
        FirebaseDatabase database = FirebaseDatabaseHelper.getFirebaseDatabaseInstance();
        databaseReference = database.getReference().child(databaseName).child(tableName);
    }

    public void savePayment(Payment payment){
        databaseReference.push().setValue(payment);
    }

    public void removePayment(Payment payment){
        databaseReference.child(payment.getFirebaseKey()).removeValue();
    }

}
