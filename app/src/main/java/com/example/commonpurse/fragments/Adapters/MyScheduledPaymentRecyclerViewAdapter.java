package com.example.commonpurse.fragments.adapters;

import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.commonpurse.helpers.constants.Constants;
import com.example.commonpurse.helpers.stringFormatters.TwoDecimalFormatter;
import com.example.commonpurse.interfaces.OnScheduledPaymentClickListener;
import com.example.commonpurse.R;
import com.example.commonpurse.customClasses.payments.ScheduledPayment;

import java.util.ArrayList;
import java.util.List;

public class MyScheduledPaymentRecyclerViewAdapter extends RecyclerView.Adapter<MyScheduledPaymentRecyclerViewAdapter.ViewHolder> {

    private List<ScheduledPayment> scheduledPayments = new ArrayList<>();
    private final OnScheduledPaymentClickListener mListener;

    private int selectedPaymentPosition = -1;

    public MyScheduledPaymentRecyclerViewAdapter(OnScheduledPaymentClickListener listener) {
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_scheduledpayment_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.scheduledPayment = scheduledPayments.get(position);

        holder.amountTv.setText(TwoDecimalFormatter.formatAmount(
                holder.scheduledPayment.getPayments().get(0).getCurrency(),
                holder.scheduledPayment.getAmount()));

        holder.paymentNameTv.setText(holder.scheduledPayment.getName());

        String paymentFrequencyTvText = holder.view.getContext().getString(R.string.this_payment_is_scheduled_every,
                holder.scheduledPayment.getIntervalFrequency(),
                Constants.getIntervalSizeString(holder.scheduledPayment.getIntervalSize()));

        holder.paymentFrequencyTv.setText(paymentFrequencyTvText);

        holder.nrOfPaymentsTv.setText(holder.view.getContext().getString(R.string.nr_of_recipients,
                holder.scheduledPayment.getPayments().size()));

        holder.view.setOnClickListener(v -> {
            if (null != mListener) {
                // Notify the active callbacks interface (the activity, if the
                // fragment is attached to one) that an item has been selected.
                selectedPaymentPosition = position;
                notifyDataSetChanged();

                mListener.onScheduledPaymentClick(holder.scheduledPayment);
            }
        });

        if (selectedPaymentPosition == position) {
            holder.view.setElevation(0f);
            holder.view.setBackgroundColor(Color.parseColor("#33CCCCCC"));
        } else {
            holder.view.setElevation(holder.view.getContext().getResources().getDimension(R.dimen.normal_margin));
            holder.view.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }
    }

    @Override
    public int getItemCount() {
        return scheduledPayments.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View view;
        public ScheduledPayment scheduledPayment;
        public final TextView amountTv, nrOfPaymentsTv, paymentNameTv, paymentFrequencyTv;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            amountTv = view.findViewById(R.id.scheduled_payment_amount_tv);
            nrOfPaymentsTv = view.findViewById(R.id.scheduled_payment_nr_of_recipients_tv);
            paymentNameTv = view.findViewById(R.id.scheduled_payment_description_tv);
            paymentFrequencyTv = view.findViewById(R.id.scheduled_payment_interval_tv);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    public void setData(ArrayList<ScheduledPayment> scheduledPayments){
        this.scheduledPayments = scheduledPayments;
        notifyDataSetChanged();
    }
}
