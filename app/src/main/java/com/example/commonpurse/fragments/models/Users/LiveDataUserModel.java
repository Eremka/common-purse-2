package com.example.commonpurse.fragments.models.Users;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;

import com.example.commonpurse.helpers.databaseHelper.FirebaseDatabaseHelper;
import com.example.commonpurse.customClasses.users.User;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class LiveDataUserModel extends LiveData<ArrayList<User>> {

    private static final String TAG = LiveDataUserModel.class.getSimpleName();

    private static String databaseType = null;
    private static String databaseName = null;

    private static FirebaseDatabase database;
    private static DatabaseReference databaseReference;

    private ArrayList<User> userList = new ArrayList<>();

    public  LiveDataUserModel (String databaseN, String tableN){
        Log.d(TAG, "LiveDataUserModel called: ");
            databaseName = databaseN;
            databaseType = tableN;

            setupDatabase();
    }

    private static void setupDatabase(){
        database = FirebaseDatabaseHelper.getFirebaseDatabaseInstance();
        databaseReference = database.getReference().child(databaseName).child(databaseType);
        databaseReference.keepSynced(true);
    }

    @Override
    protected void onActive() {
        super.onActive();
        databaseReference.addChildEventListener(childEventListener);
        databaseReference.addValueEventListener(valueEventListener);
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        userList.clear();
        databaseReference.removeEventListener(valueEventListener);
        databaseReference.removeEventListener(childEventListener);
    }

    private ChildEventListener childEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            User currentUser = dataSnapshot.getValue(User.class);
            currentUser.setFirebaseKey(dataSnapshot.getKey());

            if (!userList.contains(currentUser)){
                userList.add(currentUser);
            }
        }

        @Override
        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            User currentUser = dataSnapshot.getValue(User.class);
            currentUser.setFirebaseKey(dataSnapshot.getKey());

            for (User u: userList){
                if (u.getFirebaseKey().equals(currentUser.getFirebaseKey())){
                    u.updateValues(currentUser);
                    setValue(userList);
                }
            }
        }

        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
            User currentUser = dataSnapshot.getValue(User.class);
            currentUser.setFirebaseKey(dataSnapshot.getKey());

            userList.remove(currentUser);
            setValue(userList);
        }

        @Override
        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            // Not applicable
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            Log.d(TAG, "onCancelled error: " + databaseError.getMessage());
        }
    };

    private ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            setValue(userList);
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };
}
