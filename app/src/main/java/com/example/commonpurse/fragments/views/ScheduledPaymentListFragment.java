package com.example.commonpurse.fragments.views;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.commonpurse.fragments.adapters.MyScheduledPaymentRecyclerViewAdapter;
import com.example.commonpurse.fragments.viewModels.ScheduledPaymentListViewModel;
import com.example.commonpurse.interfaces.OnScheduledPaymentClickListener;
import com.example.commonpurse.R;
import com.example.commonpurse.customClasses.payments.ScheduledPayment;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnScheduledPaymentClickListener}
 * interface.
 */
public class ScheduledPaymentListFragment extends Fragment implements OnScheduledPaymentClickListener{

    private static final String TAG = ScheduledPaymentListFragment.class.getSimpleName();

    private MyScheduledPaymentRecyclerViewAdapter adapter;

    private ScheduledPaymentListViewModel scheduledPaymentListViewModel;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ScheduledPaymentListFragment() {
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.delete_menu_delete:
                scheduledPaymentListViewModel.deleteSelectedPayment();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.delete_menu, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivity().setTitle(R.string.scheduled_payments);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_scheduledpayment_list, container, false);

        scheduledPaymentListViewModel = ViewModelProviders.of(this).get(ScheduledPaymentListViewModel.class);
        setHasOptionsMenu(true);
        setMenuVisibility(false);

        scheduledPaymentListViewModel.getSelectedPayment().observe(this, scheduledPayment -> {
            if (scheduledPayment != null){
                setMenuVisibility(true);
            } else {
                setMenuVisibility(false);
            }
        });

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            adapter = new MyScheduledPaymentRecyclerViewAdapter(this);
            recyclerView.setAdapter(adapter);
            setDataInRecyclerView();
        }
        return view;
    }

    /**
     * Setting the data in the recycler view
     */
    private void setDataInRecyclerView() {
        scheduledPaymentListViewModel.getCurrentUser().observe(this, user -> {
            if (user != null){
                scheduledPaymentListViewModel.getScheduledPayments(user).observe(this, scheduledPayments -> {
                    adapter.setData(scheduledPayments);
                });
            }
        });
    }

    /**
     * On click action for the items in the list
     *
     */
    @Override
    public void onScheduledPaymentClick(ScheduledPayment scheduledPayment) {
        scheduledPaymentListViewModel.setSelectedPayment(scheduledPayment);
    }
}
