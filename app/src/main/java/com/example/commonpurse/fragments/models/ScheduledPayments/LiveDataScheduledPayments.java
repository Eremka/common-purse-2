package com.example.commonpurse.fragments.models.ScheduledPayments;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;

import com.example.commonpurse.helpers.databaseHelper.FirebaseDatabaseHelper;
import com.example.commonpurse.customClasses.payments.ScheduledPayment;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class LiveDataScheduledPayments extends LiveData<ArrayList<ScheduledPayment>> {

    private static final String TAG = LiveDataScheduledPayments.class.getSimpleName();
    private static DatabaseReference databaseReference;

    private ArrayList<ScheduledPayment> scheduledPayments = new ArrayList<>();

    private String userId;

    public  LiveDataScheduledPayments (String databaseName, String tableName, String userId){
        setupDatabase(databaseName, tableName);
        this.userId = userId;
    }

    private static void setupDatabase(String databaseName, String tableName) {
        FirebaseDatabase database = FirebaseDatabaseHelper.getFirebaseDatabaseInstance();
        databaseReference = database.getReference().child(databaseName).child(tableName);
    }

    @Override
    protected void onActive() {
        super.onActive();
        databaseReference.addChildEventListener(childEventListener);
        databaseReference.addValueEventListener(valueEventListener);
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        databaseReference.removeEventListener(valueEventListener);
        databaseReference.removeEventListener(childEventListener);
    }

    private ChildEventListener childEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            ScheduledPayment payment = dataSnapshot.getValue(ScheduledPayment.class);

            if (payment.getCreatorId().contains(userId)) {
                payment.setFirebaseId(dataSnapshot.getKey());
                scheduledPayments.add(payment);
            }
        }

        @Override
        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String string) {
            for (ScheduledPayment s: scheduledPayments){
                if (s.getFirebaseId().equals(dataSnapshot.getKey())){
                    scheduledPayments.remove(s);
                    ScheduledPayment newPayment = dataSnapshot.getValue(ScheduledPayment.class);
                    newPayment.setFirebaseId(dataSnapshot.getKey());
                    scheduledPayments.add(newPayment);
                    return;
                }
            }
        }

        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
            for (ScheduledPayment s: scheduledPayments){
                if (s.getFirebaseId().equals(dataSnapshot.getKey())){
                    scheduledPayments.remove(s);
                    return;
                }
            }
        }

        @Override
        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            // Not possible with this program. Function is left empty.
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            Log.d(TAG, "onCancelled with error: " + databaseError.getMessage());
        }
    };

    /**
     * This value event listener is called after the child event listener. So after all the childs
     * have been added to the list of scheduled payments, the value of the live data is set.
     */
    private ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            setValue(scheduledPayments);
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            Log.d(TAG, "onCancelled with error: " + databaseError.getMessage());
        }
    };
}
