package com.example.commonpurse.fragments.views;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.commonpurse.fragments.adapters.MyPaymentRecyclerViewAdapter;
import com.example.commonpurse.fragments.viewModels.PaymentListViewModel;
import com.example.commonpurse.fragments.viewModels.UserAndDatabaseViewModel;
import com.example.commonpurse.interfaces.DrawerLocker;
import com.example.commonpurse.interfaces.OnPaymentClickListener;
import com.example.commonpurse.R;
import com.example.commonpurse.fragments.views.PaymentListFragmentDirections.ActionPaymentListFragmentToAddPaymentFragment;

import java.util.Objects;


public class PaymentListFragment extends Fragment {

    private static final String TAG = PaymentListFragment.class.getSimpleName();
    private static final int DIALOG_FRAGMENT = 100;

    private PaymentListViewModel paymentListViewModel;
    private UserAndDatabaseViewModel userAndDatabaseViewModel;
    
    private TextView emptyTv;
    private ProgressBar progressBar;

    private MyPaymentRecyclerViewAdapter paymentAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PaymentListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Making the options menu visible.
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_list, container, false);

        // Enable the DrawerLayout (scroll in menu)
        ((DrawerLocker) getActivity()).setDrawerEnabled(true);
        getActivity().setTitle(getString(R.string.all_payments));

        // Setting up the ViewModel providers
        paymentListViewModel = ViewModelProviders.of(getActivity()).get(PaymentListViewModel.class);
        userAndDatabaseViewModel = ViewModelProviders.of(this).get(UserAndDatabaseViewModel.class);

        // Set the adapter
        RecyclerView recyclerView = view.findViewById(R.id.payment_list_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        emptyTv = view.findViewById(R.id.payment_list_empty_tv);
        progressBar = view.findViewById(R.id.payment_list_progressBar);

        // Setting up the adapter. The adapter requires two
        paymentAdapter = MyPaymentRecyclerViewAdapter.getInstance();
        paymentAdapter.setOnPaymentClickListener(listener);
        paymentAdapter.setCurrency(userAndDatabaseViewModel.getCurrentCurrency());

        recyclerView.setAdapter(paymentAdapter);
        setData(null);

        return view;
    }

    private void setData(String query) {
        paymentListViewModel.getAllUsers().observe(this, users -> {
            paymentListViewModel.getAllBasePayments(query).observe(this, payments -> {
                progressBar.setVisibility(View.GONE);
                paymentAdapter.setData(payments, users);

                if (payments.size() > 0) {
                    emptyTv.setVisibility(View.GONE);
                } else {
                    emptyTv.setVisibility(View.VISIBLE);
                }
            });

        });
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.payment_list_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    /**
     * This will navigate to the query dialog fragment. When something is entered there
     * it will come back through the viewmodel.getQuery method.
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        NavigationUI.onNavDestinationSelected(item, Navigation.findNavController(getView()));

        if (item.getItemId() == R.id.queryDialogFragment){
            paymentListViewModel.getQuery().observe(this, this::setData);
        }

        return super.onOptionsItemSelected(item);
    }

    private OnPaymentClickListener listener = payment -> {
        ActionPaymentListFragmentToAddPaymentFragment directions = PaymentListFragmentDirections.actionPaymentListFragmentToAddPaymentFragment();
        directions.setPaymentId(payment.getmTime());
        Navigation.findNavController(Objects.requireNonNull(getView())).navigate(directions);
    };
}
