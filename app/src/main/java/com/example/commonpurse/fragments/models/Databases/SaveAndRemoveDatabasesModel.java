package com.example.commonpurse.fragments.models.Databases;

import com.example.commonpurse.customClasses.databases.Databases;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SaveAndRemoveDatabasesModel {

    private static final String databaseName = "Databases";
    private static SaveAndRemoveDatabasesModel instance;

    private static DatabaseReference reference;

    public static SaveAndRemoveDatabasesModel getInstance(){
        if (instance == null){
            instance = new SaveAndRemoveDatabasesModel();
        }

        setupDatabase();
        return instance;
    }

    private static void setupDatabase() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        reference = database.getReference().child(databaseName);
    }

    public void saveDatabase(Databases db){
        reference.push().setValue(db);
    }
}
