package com.example.commonpurse.fragments.adapters;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.commonpurse.helpers.stringFormatters.TwoDecimalFormatter;
import com.example.commonpurse.interfaces.OnUserClickListener;
import com.example.commonpurse.R;
import com.example.commonpurse.customClasses.users.User;

import java.util.ArrayList;


public class MyOverviewUserRecyclerViewAdapter extends RecyclerView.Adapter<MyOverviewUserRecyclerViewAdapter.ViewHolder> {

    private ArrayList<User> userList = new ArrayList<>();
    private String currency;

    private OnUserClickListener listener;

    public MyOverviewUserRecyclerViewAdapter() {
    }

    public MyOverviewUserRecyclerViewAdapter(OnUserClickListener listener, String currency){
        this.currency = currency;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_overview_user_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        User currentUser = userList.get(position);

        holder.nameTv.setText(currentUser.getName());
        holder.amountTv.setText(TwoDecimalFormatter.formatAmount(currency,
                currentUser.getUserAmount() != null ? currentUser.getUserAmount().floatValue() : 0f));

        holder.mView.setOnClickListener(v ->{
            if (listener != null){
                listener.onUserClick(currentUser);
            }
        });
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView nameTv, amountTv;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            nameTv = view.findViewById(R.id.overview_list_item_name_tv);
            amountTv = view.findViewById(R.id.overview_list_item_amount_tv);
        }
    }

    public void setData(ArrayList<User> userList){
        this.userList.clear();
        this.userList.addAll(userList);
        notifyDataSetChanged();
    }
}
