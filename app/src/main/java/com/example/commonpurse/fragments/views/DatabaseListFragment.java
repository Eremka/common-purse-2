package com.example.commonpurse.fragments.views;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.commonpurse.fragments.adapters.MyDatabaseRecyclerViewAdapter;
import com.example.commonpurse.fragments.dialogFragments.PasswordDialogFragment;
import com.example.commonpurse.fragments.viewModels.DatabaseListViewModel;
import com.example.commonpurse.fragments.viewModels.UserAndDatabaseViewModel;
import com.example.commonpurse.interfaces.DrawerLocker;
import com.example.commonpurse.interfaces.OnDatabaseClickListener;
import com.example.commonpurse.interfaces.PasswordChecker;
import com.example.commonpurse.R;
import com.example.commonpurse.customClasses.databases.Databases;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Objects;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.commonpurse.fragments.views.DatabaseListFragmentDirections.ActionDatabaseListFragmentToAddUserFragment;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link }
 * interface.
 */
public class DatabaseListFragment extends Fragment implements PasswordChecker {

    private static final String TAG = DatabaseListFragment.class.getSimpleName();

    private TextView emptyView;
    private RecyclerView recyclerView;
    private ArrayList<Databases> databasesList;
    private ProgressBar progressBar;

    private Databases database;

    private MyDatabaseRecyclerViewAdapter adapter;

    private DatabaseListViewModel databaseListViewmodel;
    private UserAndDatabaseViewModel userAndDatabaseViewModel;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public DatabaseListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_database_list, container, false);

        // Remove the drawerlayout from the fragment.
        ((DrawerLocker) getActivity()).setDrawerEnabled(false);
        getActivity().setTitle(getString(R.string.pick_a_group));

        // Set the adapter
        adapter = new MyDatabaseRecyclerViewAdapter();
        adapter.setOnDatabaseClickListener(listener);

        Context context = view.getContext();
        recyclerView = view.findViewById(R.id.database_list_rv);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        emptyView = view.findViewById(R.id.database_list_empty_view);
        progressBar = view.findViewById(R.id.database_list_progressBar);
        databasesList = new ArrayList<>();

        // Attaching the ViewModel to the fragment
        databaseListViewmodel = ViewModelProviders.of(this).get(DatabaseListViewModel.class);
        getDatabases();

        FloatingActionButton fab = view.findViewById(R.id.database_list_fab);
        fab.setOnClickListener(v -> {
            Navigation.findNavController(v).navigate(R.id.action_databaseListFragment_to_addDatabaseFragment);
        });

        return view;
    }

    private void getDatabases() {
        databaseListViewmodel.getAllDatabases().observe(this, databases -> {
            progressBar.setVisibility(View.GONE);

            if (databases.size() > 0) {
                recyclerView.setVisibility(View.VISIBLE);
                emptyView.setVisibility(View.GONE);
            } else {
                emptyView.setVisibility(View.VISIBLE);
            }

            adapter.setDatabases(databases);
        });
    }

    private OnDatabaseClickListener listener = database -> {
        if (database.getPassword() == null) {
            loginToDatabase(database);
        } else {
            this.database = database;
            checkPassword(database.getPassword());
        }
    };

    private void checkPassword(String password) {
        PasswordDialogFragment passwordDialog = new PasswordDialogFragment(this, password);
        passwordDialog.show(getFragmentManager(), null);
    }

    private void loginToDatabase(Databases database) {
        //TempImportData.importOldData(database.getName());

        userAndDatabaseViewModel = ViewModelProviders.of(this).get(UserAndDatabaseViewModel.class);

        databaseListViewmodel.getCurrentUserInDatabase(database.getName(),
                userAndDatabaseViewModel.getCurrentUserId()).observe(this, idUser -> {

            // Saving the current database in local storage so that it can be retrieved easily.
            userAndDatabaseViewModel.saveCurrentDatabase(database);

            Log.d(TAG, "loginToDatabase saved user ID: " + userAndDatabaseViewModel.getCurrentUserId());

            // Checking whether the current user is a member of this database.
            if (idUser != null && idUser.getId().contains(userAndDatabaseViewModel.getCurrentUserId())) {
                Log.d(TAG, "Callback for user in database: " + idUser.toString());
                Navigation.findNavController(Objects.requireNonNull(getView()))
                        .navigate(R.id.action_databaseListFragment_to_overviewFragment);
            } else {
                databaseListViewmodel.getCurrentUserInDatabaseWithEmail().observe(this, emailUser -> {
                    Log.d(TAG, "loginToDatabase email user: " + emailUser);
                    if (emailUser != null){
                        Navigation.findNavController(Objects.requireNonNull(getView()))
                                .navigate(R.id.action_databaseListFragment_to_overviewFragment);
                    } else {
                       ActionDatabaseListFragmentToAddUserFragment direction =
                               DatabaseListFragmentDirections.actionDatabaseListFragmentToAddUserFragment();
                       direction.setUserEmail(userAndDatabaseViewModel.getCurrentUserEmail())
                               .setUserFirebaseId(userAndDatabaseViewModel.getCurrentUserId());
                        Navigation.findNavController(Objects.requireNonNull(getView()))
                                .navigate(direction);
                    }
                });
            }
        });

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (databasesList == null || databasesList.size() == 0) {
            recyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void passwordCorrect() {
        loginToDatabase(database);
    }
}
