package com.example.commonpurse.fragments.models.ScheduledPayments;

import androidx.annotation.NonNull;

import com.example.commonpurse.helpers.databaseHelper.FirebaseDatabaseHelper;
import com.example.commonpurse.customClasses.payments.ScheduledPayment;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SaveAndUpdateScheduledPayment {

    private static SaveAndUpdateScheduledPayment instance;
    private static DatabaseReference databaseReference;

    public static SaveAndUpdateScheduledPayment getInstance(@NonNull String databaseName, @NonNull String tableName){
        if (instance == null){
            instance = new SaveAndUpdateScheduledPayment();
        }

        setupDatabase(databaseName, tableName);
        return instance;
    }

    private static void setupDatabase(String databaseName, String tableName){
        FirebaseDatabase database = FirebaseDatabaseHelper.getFirebaseDatabaseInstance();
        databaseReference = database.getReference().child(databaseName).child(tableName);
    }

    public void saveScheduledPayment(ScheduledPayment scheduledPayment){
        databaseReference.push().setValue(scheduledPayment);
    }

    public void updateScheduledPayment(ScheduledPayment scheduledPayment){
        String firebaseKey = scheduledPayment.getFirebaseId();
        scheduledPayment.setFirebaseId(null);

        databaseReference.child(firebaseKey).setValue(scheduledPayment);
    }

    public void deleteScheduledPayment(String firebaseKey){
        databaseReference.child(firebaseKey).removeValue();
    }
}
