package com.example.commonpurse.fragments.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.commonpurse.interfaces.OnDatabaseClickListener;
import com.example.commonpurse.R;
import com.example.commonpurse.customClasses.databases.Databases;
import com.example.commonpurse.helpers.timeConverter.TimeConverter;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;


public class MyDatabaseRecyclerViewAdapter extends RecyclerView.Adapter<MyDatabaseRecyclerViewAdapter.ViewHolder> {

    private static final String TAG = MyDatabaseRecyclerViewAdapter.class.getSimpleName();

    private List<Databases> allDatabases = new ArrayList<>();
    private OnDatabaseClickListener listener;

    public MyDatabaseRecyclerViewAdapter() {
        // Required empty constructor.
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_database_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Databases currentDatabase = allDatabases.get(position);

        holder.databaseName.setText(currentDatabase.getName());
        try {
            holder.databaseUserAmount.setText(String.valueOf(currentDatabase.getNrOfUsers()));
            holder.databaseDetail.setText(getDetailText(currentDatabase.getCreator(), currentDatabase.getTimeCreated()));
        } catch (Exception e){
            Log.d(TAG, "onBindViewHolder: error " + e.getMessage());
        }

        holder.mView.setOnClickListener(v -> {
            if (listener != null) {
                // Notify the active callbacks interface (the activity, if the
                // fragment is attached to one) that an item has been selected.
                listener.onDatabaseClick(currentDatabase);
            }
        });
    }

    @Override
    public int getItemCount() {
        return allDatabases.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView databaseName, databaseUserAmount, databaseDetail;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            databaseName = view.findViewById(R.id.database_list_item_database_name);
            databaseUserAmount = view.findViewById(R.id.database_list_item_nr_of_users);
            databaseDetail = view.findViewById(R.id.database_list_item_creator_details);
        }
    }

    public void setDatabases(List<Databases> databasesList){
        this.allDatabases = databasesList;
        notifyDataSetChanged();
    }

    private String getDetailText(String name, long timeCreated){
        StringBuilder sb = new StringBuilder();
        sb.append("Database created by ");
        sb.append(name);
        sb.append(" on ");
        sb.append(TimeConverter.getDateString(timeCreated));

        return sb.toString();
    }

    public void setOnDatabaseClickListener(OnDatabaseClickListener listener){
        this.listener = listener;
    }
}
