package com.example.commonpurse.fragments.repositories;

import androidx.lifecycle.LiveData;

import com.example.commonpurse.fragments.models.ScheduledPayments.LiveDataScheduledPayments;
import com.example.commonpurse.fragments.models.ScheduledPayments.SaveAndUpdateScheduledPayment;
import com.example.commonpurse.customClasses.payments.ScheduledPayment;

import java.util.ArrayList;

public class ScheduledPaymentRepository {

    private static ScheduledPaymentRepository instance;

    private static SaveAndUpdateScheduledPayment saveAndUpdateScheduledPayment;

    public static ScheduledPaymentRepository getInstance(){
        if (instance == null){
            instance = new ScheduledPaymentRepository();
        }

        return instance;
    }

    public void saveScheduledPayment(ScheduledPayment scheduledPayment, String databaseName, String tableName){
        saveAndUpdateScheduledPayment = SaveAndUpdateScheduledPayment.getInstance(databaseName, tableName);
        saveAndUpdateScheduledPayment.saveScheduledPayment(scheduledPayment);
    }

    public void updateScheduledPayment(ScheduledPayment scheduledPayment, String databaseName, String tableName){
        saveAndUpdateScheduledPayment = SaveAndUpdateScheduledPayment.getInstance(databaseName, tableName);
        saveAndUpdateScheduledPayment.updateScheduledPayment(scheduledPayment);
    }

    public LiveData<ArrayList<ScheduledPayment>> getAllScheduledPayments(String databaseName, String tableName, String userId){
        return new LiveDataScheduledPayments(databaseName, tableName, userId);
    }

    public void deleteScheduledPayment(String databaseName, String tableName, String firebaseId){
        saveAndUpdateScheduledPayment = SaveAndUpdateScheduledPayment.getInstance(databaseName, tableName);
        saveAndUpdateScheduledPayment.deleteScheduledPayment(firebaseId);
    }
}
