package com.example.commonpurse.fragments.viewModels;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;

import com.example.commonpurse.fragments.repositories.PaymentRepository;
import com.example.commonpurse.fragments.repositories.UserAndDatabaseLocalRepo;
import com.example.commonpurse.fragments.repositories.UsersRepository;
import com.example.commonpurse.helpers.stringFormatters.UserIdFormatter;
import com.example.commonpurse.R;
import com.example.commonpurse.customClasses.payments.Payment;
import com.example.commonpurse.customClasses.users.User;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class OverviewViewModel extends AndroidViewModel {

    private static final String TAG = OverviewViewModel.class.getSimpleName();

    private UserAndDatabaseLocalRepo userAndDatabaseLocalRepo;
    private PaymentRepository paymentRepository;
    private UsersRepository usersRepository;


    public OverviewViewModel(@NonNull Application application) {
        super(application);
        userAndDatabaseLocalRepo = UserAndDatabaseLocalRepo.getInstance();
        paymentRepository = PaymentRepository.getInstance();
        usersRepository = UsersRepository.getInstance();
    }

    public LiveData<ArrayList<User>> getAllUsers() {
        return Transformations.map(usersRepository.getAllUsersInDatabase(userAndDatabaseLocalRepo.getDatabaseName(getApplication()),
                getApplication().getResources().getString(R.string.user_reference_name)), users -> users);
    }

    public LiveData<ArrayList<Payment>> getPaymentData() {
        return Transformations.map(paymentRepository.getAllPayments(userAndDatabaseLocalRepo.getDatabaseName(getApplication()),
                getApplication().getResources().getString(R.string.payment_reference_name)), payments -> payments);
    }

    public LiveData<User> getCurrentUser(String databaseName) {
        return usersRepository.getUserWithId(databaseName,
                getApplication().getResources().getString(R.string.user_reference_name),
                userAndDatabaseLocalRepo.getCurrentUserID(getApplication()));
    }

    public LiveData<ArrayList<User>> calculateStandings(ArrayList<Payment> paymentList) {
        return Transformations.map(usersRepository.getAllUsersInDatabase(userAndDatabaseLocalRepo.getDatabaseName(getApplication()),
                getApplication().getResources().getString(R.string.user_reference_name)), users -> {
            for (User u : users) {
                if (u.getUserAmount() == null) {
                    u.setUserAmount(0.0d);
                }
            }

            return getStandings(paymentList, users);
        });
    }

    private ArrayList<User> getStandings(ArrayList<Payment> paymentList, ArrayList<User> users) {
        for (Payment p : paymentList) {
            for (User u : users) {
                if (UserIdFormatter.getUserId(u).contains(p.getUser())) {
                    u.setUserAmount(u.getUserAmount().add(new BigDecimal(p.getAmount())).doubleValue());
                }

                if (UserIdFormatter.getUserId(u).contains(p.getReceiver())) {
                    u.setUserAmount(u.getUserAmount().subtract(new BigDecimal(p.getAmount())).doubleValue());
                }
            }
        }

        return users;
    }

    public LiveData<ArrayList<Payment>> getUserPaymentList(ArrayList<Payment> paymentList, String currentUser) {
        return Transformations.map(usersRepository.getAllUsersInDatabase(userAndDatabaseLocalRepo.getDatabaseName(getApplication()),
                getApplication().getResources().getString(R.string.user_reference_name)), users -> {
            for (User u: users){
                if (u.getUserAmount() == null){
                    u.setUserAmount(0.0d);
                }
            }

            return calculatePayments(getStandings(paymentList, users), currentUser);
        });
    }

    private ArrayList<Payment> calculatePayments(ArrayList<User> userStandingList, String currentUser) {
        ArrayList<Payment> userPayments = new ArrayList<>();
        ArrayList<User> userList = new ArrayList<>(userStandingList);

        Collections.sort(userList, (o1, o2) -> (o2.getUserAmount().subtract(o1.getUserAmount())).intValue());

        do {
            User highestAmountUser = null;
            User lowestAmountUser = null;
            try {
                highestAmountUser = userList.get(0);
                lowestAmountUser = userList.get(userList.size() - 1);

            } catch (IndexOutOfBoundsException e) {
                Log.d(TAG, "calculatePayments error: " + e.getMessage());
            }

            // Making sure that the coming calculations can be executed.
            if (lowestAmountUser == null) {
                Log.d(TAG, "calculatePayments: lowestAmountUser is null");
                return null;
            }

            if (highestAmountUser == null) {
                Log.d(TAG, "calculatePayments, lowest amount user is null");
                return null;
            }

            Payment p1 = new Payment();
            p1.setUser(lowestAmountUser.getName());
            p1.setReceiver(highestAmountUser.getName());

            // Creating a payment when the lowest amount user has less debt than the highest amount
            // user is owed.
            if (lowestAmountUser.getUserAmount().abs().compareTo(highestAmountUser.getUserAmount()) < 0) {
                p1.setAmount(lowestAmountUser.getUserAmount().abs().doubleValue());

                // lowestAmountUser amount is always negative.
                highestAmountUser.setUserAmount(highestAmountUser.getUserAmount()
                        .add(lowestAmountUser.getUserAmount()).doubleValue());

                try {
                    userList.remove((userList.size() - 1));
                } catch (ArrayIndexOutOfBoundsException e) {
                    e.getStackTrace();
                    Log.d(TAG, "calculatePayments: Copied standing array list size is: " + userList.size()
                            + " and the index is: " + (userList.size() - 1));
                }
                userPayments.add(p1);
            }

            // Creating a payment for when the highest amount user is owed more than the lowest amount
            // user is indebted.

            else if (lowestAmountUser.getUserAmount().abs().compareTo(highestAmountUser.getUserAmount()) > 0) {
                p1.setAmount(highestAmountUser.getUserAmount().doubleValue());

                lowestAmountUser.setUserAmount(lowestAmountUser.getUserAmount()
                        .add(highestAmountUser.getUserAmount()).doubleValue());

                userList.remove(0);
                userPayments.add(p1);
            }

            // Creating a payment for when both amounts are the same.
            else if (lowestAmountUser.getUserAmount().abs().compareTo(highestAmountUser.getUserAmount()) == 0) {
                p1.setAmount(highestAmountUser.getUserAmount().doubleValue());

                userList.remove(userList.size() - 1);
                try {
                    userList.remove(0);
                } catch (IndexOutOfBoundsException e) {
                    Log.d(TAG, "calculatePayments index out of bounds:");
                }

                userPayments.add(p1);
            }
        } while (userList.size() > 1);

        Log.d(TAG, "calculatePayments: user payments size is: " + userPayments.size());

        for (Iterator<Payment> iterator = userPayments.iterator(); iterator.hasNext(); ) {
            Payment p = iterator.next();

            if (p.getAmount() == 0) {
                iterator.remove();
            }
        }

        return makePaymentListUserSpecific(userPayments, currentUser);
    }

    private ArrayList<Payment> makePaymentListUserSpecific(ArrayList<Payment> payments, String userName) {
        for (Iterator<Payment> iterator = payments.iterator(); iterator.hasNext(); ) {
            Payment p = iterator.next();

            if (p.getReceiver().equals(p.getUser())) {
                // do nothing
            } else if (p.getUser().equals(userName) || p.getReceiver().equals(userName)) {
                p.setmCreator(userName);
            } else {
                iterator.remove();
            }
        }

        //Collections.sort(payments, (p1, p2) -> (int) (p1.getAmount() - p2.getAmount()));

        return payments;
    }

}
