package com.example.commonpurse.fragments.repositories;

import androidx.lifecycle.LiveData;

import com.example.commonpurse.fragments.models.ExchangeRates.LiveDataExchangeRate;

public class ExchangeRateRepository {

    private static final String TAG = ExchangeRateRepository.class.getSimpleName();
    private static ExchangeRateRepository instance;

    public static ExchangeRateRepository getInstance(){
        if (instance == null){
            instance = new ExchangeRateRepository();
        }

        return instance;
    }


    public LiveData<Float> getExchangeRate(String baseUrl, String baseCurrency,
                                           String convertedCurrency, String userKey){
        return new LiveDataExchangeRate(baseUrl, baseCurrency, convertedCurrency, userKey);
    }
}
