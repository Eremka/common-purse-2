package com.example.commonpurse.fragments.models.ExchangeRates;

import android.util.Log;

import androidx.lifecycle.LiveData;

import com.example.commonpurse.interfaces.apis.ExchangeRateApi;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LiveDataExchangeRate extends LiveData<Float>{

    private static final String TAG = LiveDataExchangeRate.class.getSimpleName();

    private static final String COMPACT = "ultra";
    private String baseUrl, currencies, userKey;

    public LiveDataExchangeRate(String baseUrl, String baseCurrency, String convertedCurrency, String userKey){
        this.currencies = baseCurrency + "_" + convertedCurrency;
        this.baseUrl = baseUrl;
        this.userKey = userKey;
        executeCall();
    }

    private void executeCall() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ExchangeRateApi exchangeRateApi = retrofit.create(ExchangeRateApi.class);
        Call<Map<String, Float>> call = exchangeRateApi.getExchangeRate(currencies, COMPACT, userKey);
        call.enqueue(new Callback<Map<String, Float>>() {
            @Override
            public void onResponse(Call<Map<String, Float>> call, Response<Map<String, Float>> response) {
                if (response.isSuccessful()){
                    setValue(response.body().get(currencies));
                } else {
                    setValue((float) 0);
                }
            }

            @Override
            public void onFailure(Call<Map<String, Float>> call, Throwable t) {
                Log.d(TAG, "onFailure call failed: " + t.getMessage());
                setValue((float) 0);
            }
        });
    }


}
