package com.example.commonpurse.Fragments.Repositories;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.example.commonpurse.R;

public class UserAndDatabaseLocalRepo {

    private static UserAndDatabaseLocalRepo instance;
    private static SharedPreferences prefs;

    public static UserAndDatabaseLocalRepo getInstance(){
        if (instance == null){
            instance = new UserAndDatabaseLocalRepo();
        }

        return instance;
    }

    private static SharedPreferences getSharedPrefs(Context context){
        if (prefs == null){
            prefs = context.getSharedPreferences(
                    context.getString(R.string.database_preferences), Context.MODE_PRIVATE);
        }

        return prefs;
    }

    @SuppressLint("ApplySharedPref")
    public void saveDatabaseNameAndCurrency(Context context, String name, String currency){
        prefs = getSharedPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(context.getString(R.string.last_accessed_database_name), name);
        editor.putString(context.getString(R.string.last_accessed_database_currency), currency);

        // Using commit to commit the changes to the shared preferences as the value can be
        // called rather quickly. Commit is synchronous.
        editor.commit();
    }

    @SuppressLint("ApplySharedPref")
    public void saveCurrentUserId(Context context, String id){
        prefs = getSharedPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(context.getString(R.string.current_user_id), id);

        editor.commit();
    }

    @SuppressLint("ApplySharedPref")
    public void saveCurrentUserEmail(Context context, String email) {
        prefs = getSharedPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(context.getResources().getString(R.string.current_user_email), email);

        editor.commit();
    }

    public String getCurrentUserID(Context context){
        prefs = getSharedPrefs(context);
        return prefs.getString(context.getString(R.string.current_user_id), null);
    }

    public String getDatabaseName(Context context){
        prefs = getSharedPrefs(context);
        return prefs.getString(context.getString(R.string.last_accessed_database_name), null);
    }

    public String getDatabaseCurrency(Context context){
        prefs = getSharedPrefs(context);
        return prefs.getString(context.getString(R.string.last_accessed_database_currency), null);
    }

    public String getCurrentUserEmail(Context context){
        prefs = getSharedPrefs(context);
        return prefs.getString(context.getResources().getString(R.string.current_user_email), null);
    }

}
