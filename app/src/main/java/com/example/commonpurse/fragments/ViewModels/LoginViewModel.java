package com.example.commonpurse.fragments.viewModels;

import android.app.Application;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.navigation.Navigation;

import com.example.commonpurse.fragments.repositories.UserAndDatabaseLocalRepo;
import com.example.commonpurse.R;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

public class LoginViewModel extends AndroidViewModel {

    private static final String TAG = LoginViewModel.class.getSimpleName();

    private GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getApplication().getString(R.string.default_web_client_id))
            .requestEmail()
            .build();
    private UserAndDatabaseLocalRepo userAndDatabaseLocalRepo;

    public LoginViewModel(@NonNull Application application) {
        super(application);

        userAndDatabaseLocalRepo = UserAndDatabaseLocalRepo.getInstance();
    }

    public GoogleSignInClient getGoogleSignInClient(){
        return GoogleSignIn.getClient(getApplication(), gso);
    }

    public void afterLoginTransaction(View view){
        String currentDatabase = userAndDatabaseLocalRepo.getDatabaseName(getApplication());
        Log.d(TAG, "afterLoginTransaction current database: " + currentDatabase);

        if (currentDatabase != null){
            Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_overviewFragment);
        } else {
            Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_databaseListFragment);
        }
    }

}
