package com.example.commonpurse.Fragments.Adapters.ViewPagerAdapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.commonpurse.Fragments.Views.OverviewFragment.AllUserOverviewFragment;
import com.example.commonpurse.Fragments.Views.OverviewFragment.SpecificUserOverviewFragment;
import com.example.commonpurse.customClasses.Users.User;

import java.util.ArrayList;

public class OverviewPagerAdapter extends FragmentStatePagerAdapter {

    private static final String TAG = OverviewPagerAdapter.class.getSimpleName();

    private ArrayList<User> userList;

    public OverviewPagerAdapter(@NonNull FragmentManager fm, ArrayList<User> userList) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.userList = userList;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            // First fragment is the overview fragment for all users
            return new AllUserOverviewFragment();
        } else {
            // All the other users are the specific fragments from the users from the user list.
            return new SpecificUserOverviewFragment(userList.get(position - 1));
        }
    }

    @Override
    public int getCount() {
        return userList.size() + 1;
    }


}
