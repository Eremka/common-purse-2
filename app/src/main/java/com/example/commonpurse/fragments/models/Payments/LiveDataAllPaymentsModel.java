package com.example.commonpurse.fragments.models.Payments;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;

import com.example.commonpurse.helpers.databaseHelper.FirebaseDatabaseHelper;
import com.example.commonpurse.customClasses.payments.Payment;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class LiveDataAllPaymentsModel extends LiveData<ArrayList<Payment>> {

    private static final String TAG = LiveDataAllPaymentsModel.class.getSimpleName();

    private static String databaseType = null;
    private static String databaseName = null;

    private static FirebaseDatabase database;
    private static DatabaseReference databaseReference;

    private ArrayList<Payment> paymentList = new ArrayList<>();

    public LiveDataAllPaymentsModel (String databaseN, String tableN){
            databaseName = databaseN;
            databaseType = tableN;

            setupDatabase();
    }

    private static void setupDatabase(){
        Log.d(TAG, "setupDatabase: called");
        database = FirebaseDatabaseHelper.getFirebaseDatabaseInstance();
        databaseReference = database.getReference().child(databaseName).child(databaseType);
        databaseReference.keepSynced(true);
    }

    @Override
    protected void onActive() {
        super.onActive();
        Log.d(TAG, "onActive: called");
        databaseReference.addChildEventListener(childEventListener);
        databaseReference.addListenerForSingleValueEvent(valueEventListener);
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        paymentList.clear();

        databaseReference.removeEventListener(childEventListener);
        databaseReference.removeEventListener(valueEventListener);
    }

    private ChildEventListener childEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            Payment currentPayment = dataSnapshot.getValue(Payment.class);
            currentPayment.setFirebaseKey(dataSnapshot.getKey());

            if (!paymentList.contains(currentPayment)){
                paymentList.add(currentPayment);
            }
        }

        @Override
        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            Payment currentPayment = dataSnapshot.getValue(Payment.class);
            currentPayment.setFirebaseKey(dataSnapshot.getKey());

            for (Payment p: paymentList){
                if (p.getFirebaseKey().equals(currentPayment.getFirebaseKey())){
                    p.updateValues(currentPayment);
                    setValue(paymentList);
                }
            }
        }

        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
            Payment currentPayment = dataSnapshot.getValue(Payment.class);
            currentPayment.setFirebaseKey(dataSnapshot.getKey());

            paymentList.remove(currentPayment);
            setValue(paymentList);
        }

        @Override
        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            // Not applicable
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            Log.d(TAG, "onCancelled error: " + databaseError.getMessage());
        }
    };

    private ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            setValue(paymentList);
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

}
