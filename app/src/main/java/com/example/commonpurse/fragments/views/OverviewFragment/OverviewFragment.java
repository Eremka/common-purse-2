package com.example.commonpurse.fragments.views.OverviewFragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.commonpurse.fragments.adapters.ViewPagerAdapters.OverviewPagerAdapter;
import com.example.commonpurse.fragments.viewModels.OverviewViewModel;
import com.example.commonpurse.fragments.viewModels.UserAndDatabaseViewModel;
import com.example.commonpurse.interfaces.DrawerLocker;
import com.example.commonpurse.R;
import com.example.commonpurse.customClasses.users.User;

import java.util.ArrayList;

public class OverviewFragment extends Fragment {

    private static final String TAG = OverviewFragment.class.getSimpleName();

    private OverviewViewModel overviewViewModel;
    private UserAndDatabaseViewModel userAndDatabaseViewModel;

    private ViewPager viewPager;
    private ProgressBar progressBar;
    private OverviewPagerAdapter adapter;

    private ArrayList<User> userList;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public OverviewFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.overview_menu, menu);

        getActivity().setTitle(getString(R.string.overview));
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        NavigationUI.onNavDestinationSelected(item, Navigation.findNavController(getView()));
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_overview, container, false);

        // Enable the DrawerLayout (scroll in menu)
        ((DrawerLocker) getActivity()).setDrawerEnabled(true);

        overviewViewModel = ViewModelProviders.of(this).get(OverviewViewModel.class);
        userAndDatabaseViewModel = ViewModelProviders.of(this).get(UserAndDatabaseViewModel.class);

        viewPager = view.findViewById(R.id.overview_fragment_viewpager);
        progressBar = view.findViewById(R.id.overview_fragment_progressbar);

        setViewPagerAdapter();
        setUserDataInDrawer();

        return view;
    }

    /**
     * It loads the users, and creates a new fragment for all of them in the view pager. The
     * first fragment is always the overview fragment for all users.
     */
    private void setViewPagerAdapter() {
        overviewViewModel.getAllUsers().observe(this, users -> {
            userList = users;
            adapter = new OverviewPagerAdapter(getChildFragmentManager(), userList);
            viewPager.setAdapter(adapter);
            progressBar.setVisibility(View.GONE);
        });
    }

    private void setUserDataInDrawer() {
        overviewViewModel.getCurrentUser(userAndDatabaseViewModel.getCurrentDatabase())
                .observe(this, user -> {
                    ((DrawerLocker) getActivity()).setUser(user);
        });
    }

    /**
     * When the user clicks on a name in the overview, the viewpager will go to that person.
     * @param user
     */
    public void goToUser(User user) {
        for (User u: userList){
            if (u.getId().equals(user.getId())){
                viewPager.setCurrentItem(userList.indexOf(u) + 1);
            }
        }
    }

    /**
     * Sets the viewpager to the first position if it is not there. Else, the normal onBackClicked
     * behavior happens
     * @return
     */
    public Boolean goToStart(){
        if (viewPager.getCurrentItem() != 0) {
            viewPager.setCurrentItem(0);
            return true;
        } else {
            return false;
        }
    }
}
