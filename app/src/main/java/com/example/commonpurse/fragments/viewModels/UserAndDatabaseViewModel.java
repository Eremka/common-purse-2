package com.example.commonpurse.Fragments.ViewModels;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.example.commonpurse.Fragments.Repositories.UserAndDatabaseLocalRepo;
import com.example.commonpurse.customClasses.Databases.Databases;
import com.google.firebase.auth.FirebaseUser;

public class UserAndDatabaseViewModel extends AndroidViewModel {

    private static final String TAG = UserAndDatabaseViewModel.class.getSimpleName();

    private String userId;
    private String userEmail;

    private String currentDatabase;
    private String currency;
    private UserAndDatabaseLocalRepo userAndDatabaseLocalRepo;

    public UserAndDatabaseViewModel(@NonNull Application application) {
        super(application);
        userAndDatabaseLocalRepo = UserAndDatabaseLocalRepo.getInstance();
    }

    public String getCurrentDatabase() {
        if (currentDatabase == null){
            currentDatabase = userAndDatabaseLocalRepo.getDatabaseName(getApplication());
        }

        return currentDatabase;
    }

    public String getCurrentCurrency(){
        if (currency == null){
            currency = userAndDatabaseLocalRepo.getDatabaseCurrency(getApplication());
        }

        return currency;
    }

    public void saveCurrentDatabase(Databases currentDatabase) {
        if (currentDatabase == null){
            userAndDatabaseLocalRepo.saveDatabaseNameAndCurrency(getApplication(), null, null);
        } else {
            this.currentDatabase = currentDatabase.getName();
            this.currency = currentDatabase.getCurrency();

            userAndDatabaseLocalRepo.saveDatabaseNameAndCurrency(
                    getApplication(), currentDatabase.getName(), currentDatabase.getCurrency());
        }
    }

    public void saveCurrentUser(FirebaseUser user){
        this.userId = user.getUid();
        this.userEmail = user.getEmail();

        userAndDatabaseLocalRepo.saveCurrentUserId(getApplication(), user.getUid());
        userAndDatabaseLocalRepo.saveCurrentUserEmail(getApplication(), user.getEmail());
    }

    public String getCurrentUserId(){
        if (userId == null){
            userId = userAndDatabaseLocalRepo.getCurrentUserID(getApplication());
        }

        return userId;
    }

    public String getCurrentUserEmail(){
        if (userEmail == null){
            userEmail = userAndDatabaseLocalRepo.getCurrentUserEmail(getApplication());
        }

        return userEmail;
    }

}
