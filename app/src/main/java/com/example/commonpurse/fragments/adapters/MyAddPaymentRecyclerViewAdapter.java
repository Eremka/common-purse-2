package com.example.commonpurse.Fragments.Adapters;

import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.commonpurse.Helpers.StringFormatters.TwoDecimalFormatter;
import com.example.commonpurse.R;
import com.example.commonpurse.customClasses.Users.User;

import java.util.ArrayList;


public class MyAddPaymentRecyclerViewAdapter extends RecyclerView.Adapter<MyAddPaymentRecyclerViewAdapter.ViewHolder> {

    private static final String TAG = MyAddPaymentRecyclerViewAdapter.class.getSimpleName();

    private ArrayList<User> userList = new ArrayList<>();

    private String currency = null;

    public MyAddPaymentRecyclerViewAdapter() {

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_add_payment_list_item, parent, false);
        return new ViewHolder(view, new MyCustomTextListener());
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        User currentUser = userList.get(position);

        holder.nameTv.setText(currentUser.getName());
        if (currentUser.getUserAmount() != null) {
            holder.amountTv.setText(TwoDecimalFormatter.formatAmount(currentUser.getUserAmount().floatValue()));
        }
        holder.textWatcher.updatePosition(position);
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View view;
        public final TextView nameTv;
        public final EditText amountTv;
        public final MyCustomTextListener textWatcher;


        public ViewHolder(View view, MyCustomTextListener textWatcher) {
            super(view);
            this.textWatcher = textWatcher;

            this.view = view;
            this.nameTv = view.findViewById(R.id.add_payment_list_item_name_tv);
            this.amountTv = view.findViewById(R.id.add_payment_list_item_amount_tv);
            this.amountTv.addTextChangedListener(textWatcher);
        }
    }

    public void setData(ArrayList<User> users){
        this.userList = users;
        notifyDataSetChanged();
    }

    public void setCurrency(String currency){
        this.currency = currency;
    }

    public User removeUser(int position){
        return userList.get(position);
    }

    public ArrayList<User> getData(){
        return this.userList;
    }

    private class MyCustomTextListener implements TextWatcher{

        private int position;

        public void updatePosition(int position){
            this.position = position;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            User user = userList.get(position);
            user.setUserAmount(Float.valueOf(s.toString()));
            Log.d(TAG, "onTextChanged position: " + position + " with value of " + s.toString());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }
}
