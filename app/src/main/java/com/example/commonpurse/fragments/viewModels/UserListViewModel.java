package com.example.commonpurse.Fragments.ViewModels;

import android.app.Application;
import android.graphics.Bitmap;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.commonpurse.Fragments.Repositories.UsersRepository;
import com.example.commonpurse.R;
import com.example.commonpurse.customClasses.Users.User;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;


public class UserListViewModel extends AndroidViewModel {

    private static final String TAG = UserListViewModel.class.getSimpleName();

    private UsersRepository usersRepository;

    public UserListViewModel(@NonNull Application application) {
        super(application);
        usersRepository = UsersRepository.getInstance();
    }

    public LiveData<ArrayList<User>> getAllUsers(String databaseName){
        return usersRepository.getAllUsersInDatabase(databaseName,
                getApplication().getResources().getString(R.string.user_reference_name));
    }


    public int saveUser(String databaseName, String firstName, String lastName, String email,
                        long timeCreated, String userId, Bitmap photo) {
        int resCode = -1;

        try {
            User user = new User();
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setEmailAddress(email);
            user.setId(userId == null ? createId(timeCreated, firstName, lastName) : userId);
            user.setName(createFullName(firstName, lastName));
            user.setActive(true);
            user.setTimeCreated(timeCreated);

            usersRepository.saveNewUser(databaseName,
                    getApplication().getResources().getString(R.string.user_reference_name), user, photo);
            resCode = 1;
        } catch (IllegalArgumentException e){
            Toast.makeText(getApplication(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        return resCode;
    }

    private String createFullName(String firstName, String lastName) {
        return firstName + " " + lastName;
    }

    private String createId(long timeCreated, String firstName, String lastName) {
        return firstName + lastName + timeCreated;
    }

    public LiveData<FirebaseUser> getFirebaseUser(){
        return usersRepository.getFirebaseUser();
    }

}
