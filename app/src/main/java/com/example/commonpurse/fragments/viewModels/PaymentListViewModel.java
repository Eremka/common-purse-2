package com.example.commonpurse.Fragments.ViewModels;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import com.example.commonpurse.Fragments.Repositories.PaymentRepository;
import com.example.commonpurse.Fragments.Repositories.UserAndDatabaseLocalRepo;
import com.example.commonpurse.Fragments.Repositories.UsersRepository;
import com.example.commonpurse.R;
import com.example.commonpurse.customClasses.Payments.Payment;
import com.example.commonpurse.customClasses.Users.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PaymentListViewModel extends AndroidViewModel {

    private static final String TAG = PaymentListViewModel.class.getSimpleName();

    private MutableLiveData<String> query = new MutableLiveData<>();

    private PaymentRepository paymentRepo;
    private UsersRepository userRepo;
    private UserAndDatabaseLocalRepo userAndDatabaseLocalRepo;

    public PaymentListViewModel(@NonNull Application application) {
        super(application);
        paymentRepo = PaymentRepository.getInstance();
        userRepo = UsersRepository.getInstance();
        userAndDatabaseLocalRepo = UserAndDatabaseLocalRepo.getInstance();
    }

    public LiveData<ArrayList<User>> getAllUsers(){
        return userRepo.getAllUsersInDatabase(userAndDatabaseLocalRepo.getDatabaseName(getApplication()),
                getApplication().getResources().getString(R.string.user_reference_name));
    }

    public LiveData<ArrayList<Payment>> getAllBasePayments(String query){
        // Reversing all the payments returned as this cannot be done with a Firebase Query.
        // As this is doable with a SQL Query, this is still considered a database operation.
        return Transformations.map(paymentRepo.getBasePayments(
                userAndDatabaseLocalRepo.getDatabaseName(getApplication()),
                getApplication().getResources().getString(R.string.payment_reference_name),
                query),
                payments ->{
            Collections.sort(payments, (p1, p2) ->{
                return Long.compare(p2.getmTime(), p1.getmTime());
            });

            return payments;
        });
    }

    public MutableLiveData<String> getQuery(){
        return query;
    }
}
