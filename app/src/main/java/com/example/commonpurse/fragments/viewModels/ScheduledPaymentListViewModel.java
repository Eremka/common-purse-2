package com.example.commonpurse.Fragments.ViewModels;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.commonpurse.Fragments.Repositories.ScheduledPaymentRepository;
import com.example.commonpurse.Fragments.Repositories.UserAndDatabaseLocalRepo;
import com.example.commonpurse.Fragments.Repositories.UsersRepository;
import com.example.commonpurse.Helpers.StringFormatters.UserIdFormatter;
import com.example.commonpurse.R;
import com.example.commonpurse.customClasses.Payments.ScheduledPayment;
import com.example.commonpurse.customClasses.Users.User;

import java.util.ArrayList;

public class ScheduledPaymentListViewModel extends AndroidViewModel {

    private static final String TAG = ScheduledPaymentListViewModel.class.getSimpleName();

    private ScheduledPaymentRepository scheduledPaymentRepository;
    private UserAndDatabaseLocalRepo userAndDatabaseLocalRepo;
    private UsersRepository usersRepository;

    private MutableLiveData<ScheduledPayment> selectedPayment = new MutableLiveData<>();
    private LiveData<ScheduledPayment> selectedPaymentLiveData = selectedPayment;

    public ScheduledPaymentListViewModel(@NonNull Application application) {
        super(application);
        scheduledPaymentRepository = ScheduledPaymentRepository.getInstance();
        userAndDatabaseLocalRepo = UserAndDatabaseLocalRepo.getInstance();
        usersRepository = UsersRepository.getInstance();
    }

    public LiveData<User> getCurrentUser() {
        return usersRepository.getUserWithId(userAndDatabaseLocalRepo.getDatabaseName(getApplication()),
                getApplication().getResources().getString(R.string.user_reference_name),
                userAndDatabaseLocalRepo.getCurrentUserID(getApplication()));
    }

    public LiveData<ArrayList<ScheduledPayment>> getScheduledPayments(User currentUser) {
        return scheduledPaymentRepository.getAllScheduledPayments(
                userAndDatabaseLocalRepo.getDatabaseName(getApplication()),
                getApplication().getResources().getString(R.string.scheduled_payment_reference_name),
                UserIdFormatter.getUserId(currentUser)
        );
    }

    public void setSelectedPayment(ScheduledPayment scheduledPayment) {
        if (selectedPayment.getValue() == null ||
                !selectedPayment.getValue().getFirebaseId().equals(scheduledPayment.getFirebaseId())) {
            this.selectedPayment.setValue(scheduledPayment);
        } else {
            this.selectedPayment.setValue(null);
        }
    }

    public void deleteSelectedPayment() {
        if (selectedPayment.getValue() != null) {
            scheduledPaymentRepository.deleteScheduledPayment(
                    userAndDatabaseLocalRepo.getDatabaseName(getApplication()),
                    getApplication().getResources().getString(R.string.scheduled_payment_reference_name),
                    selectedPayment.getValue().getFirebaseId()
            );
        }
    }

    public LiveData<ScheduledPayment> getSelectedPayment() {
        return selectedPaymentLiveData;
    }
}
