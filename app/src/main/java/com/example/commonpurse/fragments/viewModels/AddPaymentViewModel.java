package com.example.commonpurse.Fragments.ViewModels;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.commonpurse.Fragments.Repositories.PaymentRepository;
import com.example.commonpurse.Fragments.Repositories.ScheduledPaymentRepository;
import com.example.commonpurse.Fragments.Repositories.UserAndDatabaseLocalRepo;
import com.example.commonpurse.Fragments.Repositories.UsersRepository;
import com.example.commonpurse.Helpers.Calculations.PaymentCalculations;
import com.example.commonpurse.Helpers.Calculations.PaymentListCreator;
import com.example.commonpurse.Helpers.Calculations.UserCalculations;
import com.example.commonpurse.Helpers.StringFormatters.TwoDecimalFormatter;
import com.example.commonpurse.Helpers.StringFormatters.UserIdFormatter;
import com.example.commonpurse.R;
import com.example.commonpurse.customClasses.Payments.Payment;
import com.example.commonpurse.customClasses.Payments.ScheduledPayment;
import com.example.commonpurse.customClasses.Users.User;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AddPaymentViewModel extends AndroidViewModel {

    private static final String TAG = AddPaymentViewModel.class.getSimpleName();
    private UsersRepository usersRepository;
    private PaymentRepository paymentRepository;
    private ScheduledPaymentRepository scheduledPaymentRepository;
    private UserAndDatabaseLocalRepo userAndDatabaseLocalRepo;

    private int intervalSize;
    private int intervalFrequency;

    private MutableLiveData<Boolean> schedulePayment = new MutableLiveData<>();

    public AddPaymentViewModel(@NonNull Application application) {
        super(application);
        usersRepository = UsersRepository.getInstance();
        paymentRepository = PaymentRepository.getInstance();
        scheduledPaymentRepository = ScheduledPaymentRepository.getInstance();
        userAndDatabaseLocalRepo = UserAndDatabaseLocalRepo.getInstance();
    }

    public ArrayList<User> calculatePaymentsBeforeSave(ArrayList<User> userList, BigDecimal totalAmount, BigDecimal exchangeRate){
        return PaymentCalculations.calculatePaymentsBeforeSave(userList, totalAmount, exchangeRate);
    }

    public String createWarningString(List<User> userList, BigDecimal totalAmount, String currency){
        BigDecimal difference = calculateDifference(userList, totalAmount);

        StringBuilder sb = new StringBuilder();
        sb.append(getApplication().getResources().getString(R.string.payment_out_of_balance));
        sb.append(TwoDecimalFormatter.formatAmount(difference));
        sb.append(" ");
        sb.append(currency);
        sb.append(".");
        sb.append(System.getProperty("line.separator"));
        sb.append(getApplication().getResources().getString(R.string.please_adjust_amounts));

        return sb.toString();
    }

    public BigDecimal calculateDifference(List<User> userList, BigDecimal totalAmount){
        // Calculating the difference between the amounts in the user list compare to the total
        // amount. This is mainly for telling the user the difference.
        BigDecimal userAmount = BigDecimal.ZERO;
        for (User u: userList){
            userAmount = userAmount.add(u.getUserAmount());
        }
        return totalAmount.subtract(userAmount);
    }

    public ArrayList<Payment> createPaymentList(User paymentCreator, BigDecimal totalAmount, String currency, String description,
                            BigDecimal exchangeRate, long time, ArrayList<User> userList){
        return PaymentListCreator.createPaymentList(paymentCreator, totalAmount, currency, description,
                exchangeRate, time, userList);
    }

    public void saveScheduledPayment(@NonNull User paymentCreator, @NonNull ArrayList<Payment> paymentList){
        BigDecimal totalAmount = BigDecimal.ZERO;
        for (Payment p: paymentList){
            totalAmount = totalAmount.add(new BigDecimal(p.getAmount()));
        }

        ScheduledPayment scheduledPayment = new ScheduledPayment();
        scheduledPayment.setCreatorId(paymentCreator.getId());
        scheduledPayment.setCreatorName(paymentCreator.getName());
        scheduledPayment.setDatabaseName(userAndDatabaseLocalRepo.getDatabaseName(getApplication()));
        scheduledPayment.setDateCreated(System.currentTimeMillis());
        scheduledPayment.setAmount(TwoDecimalFormatter.formatAmount(totalAmount).doubleValue());
        scheduledPayment.setLastRun(System.currentTimeMillis());
        scheduledPayment.setName(paymentList.get(0).getDescription());
        scheduledPayment.setPayments(paymentList);
        scheduledPayment.setIntervalSize(intervalSize);
        scheduledPayment.setIntervalFrequency(intervalFrequency);

        scheduledPaymentRepository.saveScheduledPayment(scheduledPayment,
                userAndDatabaseLocalRepo.getDatabaseName(getApplication()),
                getApplication().getResources().getString(R.string.scheduled_payment_reference_name));
    }

    public void savePayments(ArrayList<Payment> paymentList, ArrayList<Payment> originalPaymentList,
                             String databaseName, boolean isScheduled, User scheduledPaymentUser){
        // Removing the payments if there are any. The payments will be recreated rather than updated
        // due to the amount in users that might differ between the payments.
        paymentRepository.removePayment(databaseName,
                getApplication().getResources().getString(R.string.payment_reference_name), originalPaymentList);

        paymentRepository.savePaymentList(databaseName,
                getApplication().getResources().getString(R.string.payment_reference_name), paymentList);

        if (isScheduled){
            saveScheduledPayment(scheduledPaymentUser, paymentList);
        }
    }

    public LiveData<ArrayList<User>> getAllUsers(String databaseName){
        return usersRepository.getAllUsersInDatabase(databaseName,
                getApplication().getResources().getString(R.string.user_reference_name));
    }

    public LiveData<ArrayList<Payment>> getPaymentsWithId(String databaseName, long id){
        return paymentRepository.getPaymentWithId(databaseName,
                getApplication().getResources().getString(R.string.payment_reference_name), id);
    }

    public ArrayList<User> getRemovedUsers(ArrayList<User> users, ArrayList<Payment> payments) {
        ArrayList<User> usersInList = new ArrayList<>(users);

        for (User u: users){
            for (Payment p: payments){
                if (UserIdFormatter.getUserId(u).contains(p.getReceiver())){
                    usersInList.remove(u);
                    Log.d(TAG, "getRemovedUsers removed User: " + u.toString());
                }
            }
        }

        return usersInList;
    }

    public void deletePayment(String databaseName, ArrayList<Payment> originalPaymentList) {
        if (originalPaymentList != null || originalPaymentList.size() != 0){
            paymentRepository.removePayment(databaseName, getApplication().getResources().
                    getString(R.string.payment_reference_name), originalPaymentList);
        }
    }

    public ArrayList<User> prepareUsers(BigDecimal paymentAmount, ArrayList<User> userList,
                                        ArrayList<User> removedUserList, ArrayList<Payment> originalPaymentList) {

        return UserCalculations.createUserListWithAmount(paymentAmount, userList, removedUserList, originalPaymentList);
    }

    public LiveData<User> getUserWithId(String currentDatabase, String currentUserId) {
        return usersRepository.getUserWithId(currentDatabase,
                getApplication().getResources().getString(R.string.user_reference_name), currentUserId);
    }

    public void setIntervalSize(int intervalSize) {
        this.intervalSize = intervalSize;
    }

    public void setIntervalFrequency(int intervalFrequency) {
        this.intervalFrequency = intervalFrequency;
    }

    public MutableLiveData<Boolean> isScheduledPayment(){
        return schedulePayment;
    }

    public void setSchedulePayment(Boolean isScheduled){
        schedulePayment.setValue(isScheduled);
    }
}
