package com.example.commonpurse.Fragments.DialogFragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.NumberPicker;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.commonpurse.Fragments.ViewModels.AddPaymentViewModel;
import com.example.commonpurse.Helpers.Constants.Constants;
import com.example.commonpurse.R;

import java.util.Objects;

public class SchedulePaymentDialogFragment extends DialogFragment {

    private static final String TAG = SchedulePaymentDialogFragment.class.getSimpleName();
    private int spinnerSelectedValue, intervalValue = 1;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_fragment_schedule_payment, null);
        builder.setView(view);
        builder.setTitle(getActivity().getString(R.string.schedule_payment_dialog_title));

        setupNumberPicker(view.findViewById(R.id.schedule_payment_number_picker));
        setupSpinner(view.findViewById(R.id.schedule_payment_spinner));

        builder.setPositiveButton(getActivity().getString(R.string.schedule), (dialog, which) -> {
            AddPaymentViewModel addPaymentViewModel =
                    ViewModelProviders.of(getActivity()).get(AddPaymentViewModel.class);
            addPaymentViewModel.setIntervalSize(spinnerSelectedValue);
            addPaymentViewModel.setIntervalFrequency(intervalValue);
            addPaymentViewModel.setSchedulePayment(true);
        });

        builder.setNegativeButton(getActivity().getString(R.string.cancel), (dialog, which) -> {
            dialog.cancel();
        });

        return builder.create();
    }

    private void setupNumberPicker(NumberPicker numberPicker) {
        numberPicker.setMinValue(Constants.NUMBER_PICKER_MIN_VALUE);
        numberPicker.setMaxValue(Constants.NUMBER_PICKER_MAX_VALUE);
        numberPicker.setOnValueChangedListener((picker, oldVal, newVal) -> {
            intervalValue = newVal;
            Log.d(TAG, "setupNumberPicker new value: " + intervalValue);
        });
        numberPicker.setValue(1);
    }

    private void setupSpinner(Spinner intervalSpinner) {
        intervalSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerSelectedValue = getIntervalValue((String)intervalSpinner.getItemAtPosition(position));
                Log.d(TAG, "onItemSelected value: " + spinnerSelectedValue);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Deliberately left empty.
            }
        });
        intervalSpinner.setSelection(0);
    }

    private int getIntervalValue(String interval) {
        switch (interval) {
            case Constants.DAYS_STRING:
                return Constants.DAYS;
            case Constants.WEEKS_STRING:
                return Constants.WEEKS;
            case Constants.MONTHS_STRING:
                return Constants.MONTHS;
            default:
                return -1;
        }
    }
}
