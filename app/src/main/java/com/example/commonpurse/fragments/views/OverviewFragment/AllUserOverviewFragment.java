package com.example.commonpurse.fragments.views.OverviewFragment;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.commonpurse.fragments.adapters.MyOverviewUserRecyclerViewAdapter;
import com.example.commonpurse.fragments.viewModels.OverviewViewModel;
import com.example.commonpurse.fragments.viewModels.UserAndDatabaseViewModel;
import com.example.commonpurse.interfaces.OnUserClickListener;
import com.example.commonpurse.R;
import com.example.commonpurse.customClasses.users.User;

import de.hdodenhof.circleimageview.CircleImageView;

public class AllUserOverviewFragment extends Fragment implements OnUserClickListener {

    private static final String TAG = AllUserOverviewFragment.class.getSimpleName();

    private TextView titleTv;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private CircleImageView profileImage;

    private MyOverviewUserRecyclerViewAdapter adapter;

    private OverviewViewModel overviewViewModel;


    public AllUserOverviewFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_user_overview, container, false);

        overviewViewModel = ViewModelProviders.of(this).get(OverviewViewModel.class);
        UserAndDatabaseViewModel userAndDatabaseViewModel = ViewModelProviders.of(this).get(UserAndDatabaseViewModel.class);

        // Set the adapter
        if (view instanceof RelativeLayout) {
            Context context = view.getContext();
            titleTv = view.findViewById(R.id.all_user_overview_name_tv);
            profileImage = view.findViewById(R.id.all_user_overview_profile_picture);
            progressBar = view.findViewById(R.id.all_user_overview_progressBar);

            adapter = new MyOverviewUserRecyclerViewAdapter(this,
                    userAndDatabaseViewModel.getCurrentCurrency());

            recyclerView = view.findViewById(R.id.all_user_overview_user_list);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(adapter);

            titleTv.setText(getString(R.string.overview));
            profileImage.setVisibility(View.GONE);
        }

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        setData();
    }

    private void setData() {
        overviewViewModel.getPaymentData().observe(this, payments -> {
            overviewViewModel.calculateStandings(payments).observe(this, users ->
                    adapter.setData(users));

            progressBar.setVisibility(View.GONE);

            recyclerView.setVisibility(View.VISIBLE);
        });

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onUserClick(User user) {
        OverviewFragment parent = (OverviewFragment) getParentFragment();
        parent.goToUser(user);
    }
}
