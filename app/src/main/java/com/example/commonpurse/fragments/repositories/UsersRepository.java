package com.example.commonpurse.Fragments.Repositories;

import android.graphics.Bitmap;

import androidx.lifecycle.LiveData;

import com.example.commonpurse.Fragments.Models.Users.LiveDataFirebaseUser;
import com.example.commonpurse.Fragments.Models.Users.LiveDataUserModel;
import com.example.commonpurse.Fragments.Models.Users.LiveDataUserWithEmailModel;
import com.example.commonpurse.Fragments.Models.Users.LiveDataUserWithIdModel;
import com.example.commonpurse.Fragments.Models.Users.SaveUserModel;
import com.example.commonpurse.customClasses.Users.User;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;

public class UsersRepository {

    private static final String TAG = UsersRepository.class.getSimpleName();

    private static UsersRepository instance;

    public static UsersRepository getInstance(){
        if (instance == null){
            instance = new UsersRepository();
        }

        return instance;
    }

    public LiveData<ArrayList<User>> getAllUsersInDatabase(String databaseName, String tableName){
        return new LiveDataUserModel(databaseName, tableName);
    }

    public LiveData<User> getUserWithId(String databaseName, String tableName, String id){
        return new LiveDataUserWithIdModel(databaseName, tableName, id);
    }

    public LiveData<User> getUserWithEmail(String databaseName, String tableName, String email){
        return new LiveDataUserWithEmailModel(databaseName, tableName, email);
    }

    public void saveNewUser(String databaseName, String tableName, User user, Bitmap photo) {
        SaveUserModel.getInstance(databaseName, tableName).saveNewUser(photo, user);
    }

    public void updateUser(String databaseName, String tableName, User user){
        SaveUserModel.getInstance(databaseName, tableName).updateUser(user);
    }

    public LiveData<FirebaseUser> getFirebaseUser() {
        return new LiveDataFirebaseUser();
    }
}
