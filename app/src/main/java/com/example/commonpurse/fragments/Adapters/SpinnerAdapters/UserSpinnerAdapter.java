package com.example.commonpurse.fragments.adapters.SpinnerAdapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.commonpurse.customClasses.users.User;

import java.util.List;

public class UserSpinnerAdapter extends ArrayAdapter<User> {

    private static final String TAG = UserSpinnerAdapter.class.getSimpleName();
    private List<User> userList;

    public UserSpinnerAdapter(@NonNull Context context, @NonNull List<User> objects) {
        super(context, 0, objects);
        userList = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    private View initView(int position, @Nullable View convertView, @NonNull ViewGroup parent){
        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(
                    android.R.layout.simple_spinner_dropdown_item, parent, false);
        }

        TextView nameTv = convertView.findViewById(android.R.id.text1);
        nameTv.setText(getItem(position).getName());
        Log.d(TAG, "initView user: " + getItem(position).toString());
        Log.d(TAG, "initView position: " + position);

        return convertView;
    }

    public int getPositionFromName(String userName){
        int position = -1;

        for (User u: userList){
            if (u.getName().equals(userName)){
                position = userList.indexOf(u);
                break;
            }
        }

        return position;
    }
}
