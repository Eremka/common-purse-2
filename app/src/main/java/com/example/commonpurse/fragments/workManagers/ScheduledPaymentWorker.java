package com.example.commonpurse.fragments.workManagers;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.example.commonpurse.fragments.repositories.PaymentRepository;
import com.example.commonpurse.fragments.repositories.ScheduledPaymentRepository;
import com.example.commonpurse.fragments.repositories.UserAndDatabaseLocalRepo;
import com.example.commonpurse.helpers.constants.Constants;
import com.example.commonpurse.helpers.databaseHelper.FirebaseDatabaseHelper;
import com.example.commonpurse.MainActivity;
import com.example.commonpurse.R;
import com.example.commonpurse.customClasses.payments.Payment;
import com.example.commonpurse.customClasses.payments.ScheduledPayment;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.concurrent.CountDownLatch;

public class ScheduledPaymentWorker extends Worker {

    private CountDownLatch countDownLatch = new CountDownLatch(1);
    private static final String TAG = ScheduledPaymentWorker.class.getSimpleName();
    private UserAndDatabaseLocalRepo userAndDatabaseLocalRepo;
    private String currentUserId;

    private Result result = null;
    private int updatedPayments = 0;
    private static final String NOTIFICATION_ID = "ScheduledPaymentNotification";
    private static final String CHANNEL_ID = "ScheduledPaymentNotificationChannel";
    private static final int NOTIFICATION_INT_ID = 123;

    public ScheduledPaymentWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        userAndDatabaseLocalRepo = UserAndDatabaseLocalRepo.getInstance();
        currentUserId = userAndDatabaseLocalRepo.getCurrentUserID(context);
    }

    @NonNull
    @Override
    public Result doWork() {
        createNotificationChannel();

        DatabaseReference databaseReference = getDatabaseReference();
        databaseReference.addListenerForSingleValueEvent(valueEventListener);

        try {
            countDownLatch.await();
            return result;
        } catch (InterruptedException e) {
            Log.e(TAG, "doWork error: " + e.getMessage());
            return result;
        }
    }

    private DatabaseReference getDatabaseReference() {
        String databaseName = userAndDatabaseLocalRepo.getDatabaseName(getApplicationContext());
        String tableName = getApplicationContext().getResources().getString(R.string.scheduled_payment_reference_name);

        FirebaseDatabase database = FirebaseDatabaseHelper.getFirebaseDatabaseInstance();
        return database.getReference().child(databaseName).child(tableName);
    }

    private ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            ArrayList<ScheduledPayment> scheduledPayments = new ArrayList<>();

            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                ScheduledPayment scheduledPayment = snapshot.getValue(ScheduledPayment.class);
                if (scheduledPayment.getCreatorId().contains(currentUserId)) {
                    scheduledPayments.add(scheduledPayment);
                }
            }

            checkPaymentDates(scheduledPayments);
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            result = Result.retry();
            countDownLatch.countDown();
        }
    };

    private void checkPaymentDates(ArrayList<ScheduledPayment> payments) {
        Calendar today = Calendar.getInstance(TimeZone.getDefault());

        for (ScheduledPayment payment : payments) {
            Calendar paymentDate = Calendar.getInstance();
            paymentDate.setTimeInMillis(payment.getLastRun());

            compareMonths(today, paymentDate, payment);
        }

        if (updatedPayments > 0) {
            createNotification(updatedPayments);
        }
        result = Result.success();
        countDownLatch.countDown();
    }

    private void compareMonths(Calendar today, Calendar paymentDate, ScheduledPayment scheduledPayment) {
        switch (scheduledPayment.getIntervalSize()) {
            case Constants.MONTHS:
                paymentDate.add(Calendar.MONTH, scheduledPayment.getIntervalFrequency());
                break;
            case Constants.WEEKS:
                paymentDate.add(Calendar.WEEK_OF_YEAR, scheduledPayment.getIntervalFrequency());
                break;
            case Constants.DAYS:
                paymentDate.add(Calendar.DAY_OF_YEAR, scheduledPayment.getIntervalFrequency());
        }

        if (today.get(Calendar.YEAR) > paymentDate.get(Calendar.YEAR) ||
                today.get(Calendar.DAY_OF_YEAR) >= paymentDate.get(Calendar.DAY_OF_YEAR)) {
            scheduledPayment.setLastRun(paymentDate.getTimeInMillis());

            makePayments(scheduledPayment.getPayments(), scheduledPayment.getLastRun());
            updateScheduledPayment(scheduledPayment);
        }

    }

    private void makePayments(ArrayList<Payment> payments, long lastRun) {
        PaymentRepository paymentRepository = PaymentRepository.getInstance();

        for (Payment p: payments){
            p.setmTime(lastRun);
        }

        paymentRepository.savePaymentList(userAndDatabaseLocalRepo.getDatabaseName(getApplicationContext()),
                getApplicationContext().getResources().getString(R.string.payment_reference_name),
                payments);
    }

    private void updateScheduledPayment(ScheduledPayment scheduledPayment) {
        ScheduledPaymentRepository scheduledPaymentRepository = ScheduledPaymentRepository.getInstance();

        scheduledPaymentRepository.updateScheduledPayment(scheduledPayment,
                scheduledPayment.getDatabaseName(),
                getApplicationContext().getResources().getString(R.string.scheduled_payment_reference_name));

        updatedPayments++;
    }

    private void createNotification(int updates) {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0,
                intent, 0);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
                getApplicationContext(), NOTIFICATION_ID)
                .setSmallIcon(R.drawable.ic_access_time_white_24dp)
                .setContentTitle(getApplicationContext().getResources().getString(R.string.updated_payments))
                .setContentText(getApplicationContext().getResources().getString(R.string.scheduled_payments_created, updates))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());
        notificationManager.notify(NOTIFICATION_INT_ID, notificationBuilder.build());
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getApplicationContext().getResources().getString(R.string.channel_name);
            String description = getApplicationContext().getResources().getString(R.string.channel_description);
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getApplicationContext().getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}
