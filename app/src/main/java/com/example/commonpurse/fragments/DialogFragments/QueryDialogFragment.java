package com.example.commonpurse.fragments.dialogFragments;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.commonpurse.fragments.viewModels.PaymentListViewModel;
import com.example.commonpurse.R;

public class QueryDialogFragment extends DialogFragment {

    private static final String TAG = QueryDialogFragment.class.getSimpleName();


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_fragment_query, null);
        PaymentListViewModel paymentListViewModel = ViewModelProviders.of(getActivity()).get(PaymentListViewModel.class);

        builder.setView(view);
        builder.setTitle(getResources().getString(R.string.query_dialog_fragment_title));
        builder.setPositiveButton(R.string.search, (dialog, which) -> {
            EditText queryEt = view.findViewById(R.id.dialog_fragment_query_et);
            paymentListViewModel.getQuery().setValue(queryEt.getText().toString().trim());
    });
        builder.setNegativeButton(R.string.cancel, (dialog, which) -> {
            dialog.cancel();
        });

        return builder.create();
    }
}
