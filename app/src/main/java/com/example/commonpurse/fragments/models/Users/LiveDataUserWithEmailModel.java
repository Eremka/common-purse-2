package com.example.commonpurse.fragments.models.Users;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.example.commonpurse.helpers.databaseHelper.FirebaseDatabaseHelper;
import com.example.commonpurse.customClasses.users.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class LiveDataUserWithEmailModel extends LiveData<User> {

    private static final String TAG = LiveDataUserWithIdModel.class.getSimpleName();

    private String email;
    private Query query;

    public LiveDataUserWithEmailModel(String databaseName, String tableName, String email) {
        this.email = email;

        setupDatabase(databaseName, tableName);
    }

    private void setupDatabase(String databaseName, String tableName) {
        FirebaseDatabase database = FirebaseDatabaseHelper.getFirebaseDatabaseInstance();
        DatabaseReference databaseReference = database.getReference();

        query = databaseReference.child(databaseName).child(tableName).orderByChild("emailAddress").equalTo(email);
    }

    @Override
    protected void onActive() {
        super.onActive();
        query.addListenerForSingleValueEvent(eventListener);
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        query.removeEventListener(eventListener);
    }

    private ValueEventListener eventListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            User user = null;
            for (DataSnapshot users : dataSnapshot.getChildren()) {
                user = users.getValue(User.class);

                user.setFirebaseKey(dataSnapshot.getKey());
                Log.d(TAG, "onDataChange user: " + user.toString());
            }
            setValue(user);
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            Log.d(TAG, "onCancelled: called");
        }
    };

}
