package com.example.commonpurse.fragments.repositories;

import androidx.lifecycle.LiveData;

import com.example.commonpurse.fragments.models.Databases.LiveDataDatabaseModel;
import com.example.commonpurse.customClasses.databases.Databases;
import com.example.commonpurse.fragments.models.Databases.SaveAndRemoveDatabasesModel;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class DatabasesRepository {

    private static DatabasesRepository instance;
    private static SaveAndRemoveDatabasesModel saveAndRemoveDatabasesModel;

    public static DatabasesRepository getInstance(){
        if (instance == null) {
            instance = new DatabasesRepository();
        }

        saveAndRemoveDatabasesModel = SaveAndRemoveDatabasesModel.getInstance();
        return instance;
    }

    public LiveData<List<Databases>> getAllDatabases(String databaseName){
        return LiveDataDatabaseModel.getInstance(databaseName);
    }

    public void saveDatabase(Databases db){
        saveAndRemoveDatabasesModel.saveDatabase(db);
    }
}
