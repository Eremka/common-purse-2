package com.example.commonpurse.fragments.models.Users;

import android.graphics.Bitmap;
import android.util.Log;

import com.example.commonpurse.helpers.databaseHelper.FirebaseDatabaseHelper;
import com.example.commonpurse.customClasses.users.User;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

public class SaveUserModel {

    private static final String TAG = SaveUserModel.class.getSimpleName();
    private static SaveUserModel instance;

    private static DatabaseReference databaseReference;
    private static StorageReference storageReference;

    public static SaveUserModel getInstance(String databaseN, String tableN){
        if (instance == null){
            instance = new SaveUserModel();
        }

        setupDatabase(databaseN, tableN);
        return instance;
    }

    private static void setupDatabase(String databaseName, String tableName){
        FirebaseDatabase database = FirebaseDatabaseHelper.getFirebaseDatabaseInstance();
        databaseReference = database.getReference().child(databaseName).child(tableName);

        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference().child(databaseName).child(tableName);
    }

    public void saveNewUser(Bitmap picture, User user){
        storageReference = storageReference.child(System.currentTimeMillis() + user.getId());

        if (picture != null && picture.getByteCount() != 0){
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            picture.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            UploadTask uploadTask = storageReference.putBytes(outputStream.toByteArray());
            uploadTask.addOnCompleteListener(taskSnapshot -> {
                storageReference.getDownloadUrl().addOnSuccessListener(uri -> {
                    Log.d(TAG, "onSuccess photo uri: " + uri.toString());
                    user.setPhotoUri(uri.toString());
                    databaseReference.push().setValue(user);
                });
            }).addOnFailureListener(e -> {
                Log.d(TAG, "saveNewUser failed because: " + e.getMessage());
            });
        } else {
            databaseReference.push().setValue(user);
        }
    }

    public void updateUser(User user){
        String firebaseKey = user.getFirebaseKey();
        user.setFirebaseKey(null);
        databaseReference.child(firebaseKey).setValue(user);
    }
}
