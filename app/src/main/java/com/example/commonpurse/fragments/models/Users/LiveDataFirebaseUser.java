package com.example.commonpurse.fragments.models.Users;

import androidx.lifecycle.LiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LiveDataFirebaseUser extends LiveData<FirebaseUser> {

    public LiveDataFirebaseUser(){
        FirebaseAuth fbAuth = FirebaseAuth.getInstance();
        setValue(fbAuth.getCurrentUser());
    }
}
