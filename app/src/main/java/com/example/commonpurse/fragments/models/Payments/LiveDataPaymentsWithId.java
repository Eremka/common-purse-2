package com.example.commonpurse.fragments.models.Payments;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.example.commonpurse.helpers.databaseHelper.FirebaseDatabaseHelper;
import com.example.commonpurse.customClasses.payments.Payment;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class LiveDataPaymentsWithId extends LiveData<ArrayList<Payment>> {

    private static final String TAG = LiveDataPaymentsWithId.class.getSimpleName();

    private static FirebaseDatabase database;
    private static Query databaseReference;

    public LiveDataPaymentsWithId(String databaseName, String tableName, long id){
        setupDatabase(databaseName, tableName, id);
    }

    private static void setupDatabase(String databaseName, String tableName, long id){
        database = FirebaseDatabaseHelper.getFirebaseDatabaseInstance();
        databaseReference = database.getReference().child(databaseName).child(tableName)
                .orderByChild("mTime").equalTo(id);
    }

    @Override
    protected void onActive() {
        super.onActive();
        databaseReference.addListenerForSingleValueEvent(valueEventListener);
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        databaseReference.removeEventListener(valueEventListener);
    }

    private ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            ArrayList<Payment> paymentList = new ArrayList<>();

            for (DataSnapshot payments: dataSnapshot.getChildren()){
                Payment payment = payments.getValue(Payment.class);
                payment.setFirebaseKey(payments.getKey());
                paymentList.add(payment);
            }

            setValue(paymentList);
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };
}
