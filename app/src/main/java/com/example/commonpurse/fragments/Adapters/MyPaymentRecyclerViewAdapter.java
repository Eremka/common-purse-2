package com.example.commonpurse.fragments.adapters;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.commonpurse.helpers.stringFormatters.DescriptionText;
import com.example.commonpurse.helpers.stringFormatters.TwoDecimalFormatter;
import com.example.commonpurse.helpers.stringFormatters.UserIdFormatter;
import com.example.commonpurse.interfaces.OnPaymentClickListener;
import com.example.commonpurse.R;
import com.example.commonpurse.customClasses.payments.Payment;
import com.example.commonpurse.customClasses.users.User;

import java.util.ArrayList;
import java.util.List;


public class MyPaymentRecyclerViewAdapter extends RecyclerView.Adapter<MyPaymentRecyclerViewAdapter.ViewHolder> {

    private static final String TAG = MyPaymentRecyclerViewAdapter.class.getSimpleName();
    private String currency = null;

    private List<Payment> paymentList = new ArrayList<>();
    private ArrayList<User> userList = new ArrayList<>();
    private OnPaymentClickListener listener;

    private static MyPaymentRecyclerViewAdapter adapter;

    public MyPaymentRecyclerViewAdapter() {
        // Required empty constructor.
    }

    public static MyPaymentRecyclerViewAdapter getInstance(){
        if (adapter == null){
            adapter = new MyPaymentRecyclerViewAdapter();
        }

        return adapter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_payment_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Payment currentPayment = paymentList.get(position);

        // Setting the text on the views
        holder.descriptionTv.setText(currentPayment.getDescription());
        holder.amountTv.setText(TwoDecimalFormatter.formatAmount(currency, currentPayment.getTotalAmount()));

        // Getting the user name and setting it in the detail view.
        for (User u: userList) {
            if (UserIdFormatter.getUserId(u).contains(currentPayment.getUser())) {
                holder.detailsTv.setText(DescriptionText.getDescriptionText(u.getName(),
                        currentPayment.getmTime()));
            }
        }

        // If there has been a currency conversion, the original amount shows, otherwise
        // the view is gone.
        if (currentPayment.getmOriginalAmount() != 0){
            holder.originalAmountTv.setVisibility(View.VISIBLE);
            holder.originalAmountTv.setText(TwoDecimalFormatter.formatAmount(
                    currentPayment.getCurrency(),currentPayment.getmOriginalAmount()));
        } else {
            holder.originalAmountTv.setVisibility(View.GONE);
        }

        holder.mView.setOnClickListener(v -> {
            if (listener != null){
                listener.onPaymentClick(currentPayment);
            }
        });
    }

    @Override
    public int getItemCount() {
        return paymentList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView descriptionTv, detailsTv, amountTv, originalAmountTv;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            descriptionTv = view.findViewById(R.id.payment_list_item_description_text_view);
            detailsTv = view.findViewById(R.id.payment_list_item_details_tv);
            amountTv = view.findViewById(R.id.payment_list_item_amount_text_view);
            originalAmountTv = view.findViewById(R.id.payment_list_item_original_amount_view);
        }
    }

    public void setData(List<Payment> payments, ArrayList<User> userList){
        this.paymentList = payments;
        this.userList = userList;
        notifyDataSetChanged();
    }

    public void setCurrency(String currency){
        this.currency = currency;
    }

    public void setOnPaymentClickListener(OnPaymentClickListener listener){
        this.listener = listener;
    }
}
