package com.example.commonpurse.fragments.views;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.commonpurse.fragments.viewModels.LoginViewModel;
import com.example.commonpurse.fragments.viewModels.UserAndDatabaseViewModel;
import com.example.commonpurse.interfaces.DrawerLocker;
import com.example.commonpurse.R;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

public class LoginFragment extends Fragment {

    private static final String TAG = LoginFragment.class.getSimpleName();
    private static final int RC_GOOGLE_SIGN_IN = 100;

    private FirebaseAuth fbAuth;

    private UserAndDatabaseViewModel userAndDatabaseViewModel;

    private SignInButton googleSignInButton;
    private LoginViewModel loginViewModel;
    private View view;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_login, container, false);

        // Disabling the Drawyer Layout.
        ((DrawerLocker)getActivity()).setDrawerEnabled(false);
        getActivity().setTitle(getString(R.string.login));

        googleSignInButton = view.findViewById(R.id.google_sign_in_button);
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);

        userAndDatabaseViewModel = ViewModelProviders.of(this).get(UserAndDatabaseViewModel.class);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        GoogleSignInClient gsc = loginViewModel.getGoogleSignInClient();
        GoogleSignInAccount gsa = GoogleSignIn.getLastSignedInAccount(getActivity());

        fbAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = fbAuth.getCurrentUser();

        if (currentUser == null){
            googleSignIn(gsc);
        } else {
            userAndDatabaseViewModel.saveCurrentUser(currentUser);
            Log.d(TAG, "onCreateView: current user is: " + currentUser.getUid());
            exitLoginFragment();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * Logging in with the google sign in client.
     * @param client is only GOOGLE Sign in.
     */
    private void googleSignIn(GoogleSignInClient client){
        Intent intent = client.getSignInIntent();
        startActivityForResult(intent, RC_GOOGLE_SIGN_IN);
    }

    /**
     * Getting the login results from the sign in client
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_GOOGLE_SIGN_IN ){
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
            Log.d(TAG, "onActivityResult: " + resultCode);
        }
    }

    /**
     * Handling the sign in results.
     * @param completedTask
     */
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            firebaseAuthWithGoogle(account);
            Log.d(TAG, "handleSignInResult: Sign in successful");
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.

            // Trying to login again, by beginning from the beginning.
            onCreate(null);
            Toast.makeText(getActivity(), getActivity().getString(R.string.something_has_gone_wrong_with_logging_in),
                    Toast.LENGTH_SHORT).show();
            Log.d(TAG, "handleSignInResult: " + e.getMessage());
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        fbAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithCredential:success");
                        FirebaseUser user = fbAuth.getCurrentUser();
                        userAndDatabaseViewModel.saveCurrentUser(user);
                        Log.d(TAG, "onComplete: user is: " + user.getUid());
                        exitLoginFragment();
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithCredential:failure", task.getException());
                        Snackbar.make(getActivity().findViewById(R.id.main_activity_main_layout), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                    }
                });
    }

    private void exitLoginFragment(){
        loginViewModel.afterLoginTransaction(view);
    }

}
