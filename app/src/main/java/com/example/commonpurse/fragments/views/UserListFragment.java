package com.example.commonpurse.fragments.views;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.commonpurse.fragments.adapters.MyUserListRecyclerViewAdapter;
import com.example.commonpurse.fragments.viewModels.UserAndDatabaseViewModel;
import com.example.commonpurse.fragments.viewModels.UserListViewModel;
import com.example.commonpurse.interfaces.DrawerLocker;
import com.example.commonpurse.R;


public class UserListFragment extends Fragment {

    private UserListViewModel userListViewModel;
    private UserAndDatabaseViewModel userAndDatabaseViewModel;
    private MyUserListRecyclerViewAdapter adapter;

    public UserListFragment() {
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.add_user_menu, menu);

        getActivity().setTitle(getString(R.string.all_users));
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        NavigationUI.onNavDestinationSelected(item, Navigation.findNavController(getView()));
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userListViewModel = ViewModelProviders.of(this).get(UserListViewModel.class);
        userAndDatabaseViewModel = ViewModelProviders.of(this).get(UserAndDatabaseViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_list, container, false);
        setHasOptionsMenu(true);

        ((DrawerLocker)getActivity()).setDrawerEnabled(true);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;

            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            adapter = new MyUserListRecyclerViewAdapter();
            recyclerView.setAdapter(adapter);
            loadUserData();
        }
        return view;
    }

    private void loadUserData() {
        userListViewModel.getAllUsers(userAndDatabaseViewModel.getCurrentDatabase())
                .observe(this, users -> {
                    adapter.setData(users);
                });
    }
}
