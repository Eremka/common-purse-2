package com.example.commonpurse.fragments.models.Users;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.example.commonpurse.helpers.databaseHelper.FirebaseDatabaseHelper;
import com.example.commonpurse.customClasses.users.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

public class LiveDataUserWithIdModel extends LiveData<User> {

    private static final String TAG = LiveDataUserWithIdModel.class.getSimpleName();

    private String id;
    private Query query;

    public LiveDataUserWithIdModel(String databaseName, String tableName, String id) {
        this.id = id;

        setupDatabase(databaseName, tableName);
    }

    private void setupDatabase(String databaseName, String tableName) {
        FirebaseDatabase database = FirebaseDatabaseHelper.getFirebaseDatabaseInstance();
        DatabaseReference databaseReference = database.getReference();
        databaseReference.keepSynced(true);

        query = databaseReference.child(databaseName).child(tableName);
    }

    @Override
    protected void onActive() {
        super.onActive();
        query.addListenerForSingleValueEvent(eventListener);
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        query.removeEventListener(eventListener);
    }

    private ValueEventListener eventListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            User _user = null;

            for (DataSnapshot users : dataSnapshot.getChildren()) {
                User user = users.getValue(User.class);

                if(Objects.requireNonNull(user).getId().contains(id)) {
                    user.setFirebaseKey(dataSnapshot.getKey());
                    _user = user;
                }
            }

            setValue(_user);
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            Log.d(TAG, "onCancelled: called");
        }
    };

}
