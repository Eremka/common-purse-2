package com.example.commonpurse.Fragments.Adapters;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.commonpurse.R;
import com.example.commonpurse.customClasses.Users.User;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class MyUserListRecyclerViewAdapter extends RecyclerView.Adapter<MyUserListRecyclerViewAdapter.ViewHolder> {

    private List<User> userList = new ArrayList<>();

    public MyUserListRecyclerViewAdapter() {

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_user_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        User currentUser = userList.get(position);

        holder.nameTv.setText(currentUser.getName());
        holder.emailTv.setText(currentUser.getEmailAddress());

        if (currentUser.getPhotoUri() != null){
            Glide.with(holder.mView.getContext())
                    .load(currentUser.getPhotoUri())
                    .into(holder.profileImageView);
        } else {
            Glide.with(holder.mView.getContext())
                    .load(holder.mView.getContext().getDrawable(R.drawable.smile))
                    .into(holder.profileImageView);
        }
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final CircleImageView profileImageView;
        public final TextView nameTv, emailTv;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            profileImageView = view.findViewById(R.id.user_list_item_profile_image);
            nameTv = view.findViewById(R.id.user_list_item_name_tv);
            emailTv = view.findViewById(R.id.user_list_item_email_tv);
        }

    }

    public void setData(ArrayList<User> userList){
        this.userList.clear();
        this.userList = userList;
        notifyDataSetChanged();
    }
}
