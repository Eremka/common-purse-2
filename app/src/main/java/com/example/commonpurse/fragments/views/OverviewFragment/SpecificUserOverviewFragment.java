package com.example.commonpurse.fragments.views.OverviewFragment;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.commonpurse.fragments.adapters.MyOverviewPaymentRecyclerViewAdapter;
import com.example.commonpurse.fragments.viewModels.OverviewViewModel;
import com.example.commonpurse.fragments.viewModels.UserAndDatabaseViewModel;
import com.example.commonpurse.R;
import com.example.commonpurse.customClasses.users.User;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class SpecificUserOverviewFragment extends Fragment {

    private User selectedUser;

    private TextView emptyTv, titleTv;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private CircleImageView profileImage;

    private MyOverviewPaymentRecyclerViewAdapter adapter;
    private OverviewViewModel overviewViewModel;

    public SpecificUserOverviewFragment() {
        // Required empty public constructor
    }

    public SpecificUserOverviewFragment(@NonNull User user) {
        selectedUser = user;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_user_overview, container, false);

        UserAndDatabaseViewModel userAndDatabaseViewModel = ViewModelProviders.of(this)
                .get(UserAndDatabaseViewModel.class);

        overviewViewModel = ViewModelProviders.of(this).get(OverviewViewModel.class);

        // Set the adapter
        if (view instanceof RelativeLayout) {
            Context context = view.getContext();
            emptyTv = view.findViewById(R.id.all_user_overview_empty_tv);
            titleTv = view.findViewById(R.id.all_user_overview_name_tv);
            profileImage = view.findViewById(R.id.all_user_overview_profile_picture);
            progressBar = view.findViewById(R.id.all_user_overview_progressBar);

            adapter = new MyOverviewPaymentRecyclerViewAdapter(userAndDatabaseViewModel.getCurrentCurrency());

            recyclerView = view.findViewById(R.id.all_user_overview_user_list);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(adapter);

            setUserName();
            setProfileImage();
            loadData();
        }

        return view;
    }

    private void setUserName() {
        titleTv.setText(selectedUser.getName());
    }

    private void setProfileImage() {
        if (selectedUser.getPhotoUri() != null) {
            profileImage.setVisibility(View.VISIBLE);
            Glide.with(this)
                    .load(selectedUser.getPhotoUri())
                    .into(profileImage);
        } else {
            profileImage.setVisibility(View.GONE);
        }
    }

    private void loadData() {
        overviewViewModel.getPaymentData().observe(this, payments -> {
            overviewViewModel.getUserPaymentList(payments, selectedUser.getName()).observe(this,
                    paymentList -> {
                        adapter.setData(paymentList);

                        if (payments.size() == 0) {
                            emptyTv.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                        } else {
                            emptyTv.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                        }

                        progressBar.setVisibility(View.GONE);
                    });
        });
    }

}
