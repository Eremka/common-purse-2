package com.example.commonpurse.customClasses.payments;

import android.annotation.SuppressLint;

/**
 * Created by robbe on 18-11-2017.
 */

public class Payment {

    private String currency;
    private String user;
    private String mCreator;
    private String mCreatorRole;
    private String receiver;
    private double amount;
    private double totalAmount;
    private double mOriginalAmount;
    private double exchangeRate;
    private String description;
    private long mTime;
    private String photoId;
    private boolean approved;
    private boolean basePayment;
    private String firebaseKey;

    public Payment(){}

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getmCreator() {
        return mCreator;
    }

    public void setmCreator(String mCreator) {
        this.mCreator = mCreator;
    }

    public String getmCreatorRole() {
        return mCreatorRole;
    }

    public void setmCreatorRole(String mCreatorRole) {
        this.mCreatorRole = mCreatorRole;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getmOriginalAmount() {
        return mOriginalAmount;
    }

    public void setmOriginalAmount(double mOriginalAmount) {
        this.mOriginalAmount = mOriginalAmount;
    }

    public double getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getmTime() {
        return mTime;
    }

    public void setmTime(long mTime) {
        this.mTime = mTime;
    }

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public boolean isBasePayment() {
        return basePayment;
    }

    public void setBasePayment(boolean basePayment) {
        this.basePayment = basePayment;
    }

    public String getFirebaseKey() {
        return firebaseKey;
    }

    public void setFirebaseKey(String firebaseKey) {
        this.firebaseKey = firebaseKey;
    }

    public String toShortString(){
        String[] firstName = this.getReceiver().split(" ");
        @SuppressLint("DefaultLocale") String formattedAmount = String.format("%20.2f", this.getAmount());

        return firstName[0] + formattedAmount;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Payment: ");
        builder.append(this.getDescription());
        builder.append(" for: ");
        builder.append(this.getAmount());
        builder.append(System.getProperty("line.separator"));
        builder.append("At time: ");
        //builder.append(this.getTime());
        builder.append(System.getProperty("line.separator"));
        builder.append("From: ");
        builder.append(this.getUser());
        builder.append(" to: ");
        builder.append(this.getReceiver());

        return builder.toString();
    }

    public String toLongString(){
        StringBuilder builder = new StringBuilder();
        builder.append("Your payment is: ");
        builder.append(System.getProperty("line.separator"));
        builder.append("User: ");
        builder.append(this.getUser());
        builder.append(System.getProperty("line.separator"));
        builder.append("Creator: ");
        builder.append(this.getmCreator());
        builder.append(System.getProperty("line.separator"));
        builder.append("Creator role: ");
        //builder.append(this.getCreatorRole());
        builder.append(System.getProperty("line.separator"));
        builder.append("Receiver: ");
        builder.append(this.getReceiver());
        builder.append(System.getProperty("line.separator"));
        builder.append("Currency: ");
        builder.append(this.getCurrency());
        builder.append(System.getProperty("line.separator"));
        builder.append("Amount: ");
        builder.append(this.getAmount());
        builder.append(System.getProperty("line.separator"));
        builder.append("Total Amount: ");
        builder.append(this.getTotalAmount());
        builder.append(System.getProperty("line.separator"));
        builder.append("Amount in other currency: ");
        //builder.append(this.getOriginalAmount());
        builder.append(System.getProperty("line.separator"));
        builder.append("Description: ");
        builder.append(this.getDescription());
        builder.append(System.getProperty("line.separator"));
        builder.append("Time; ");
        builder.append(this.getmTime());
        builder.append(System.getProperty("line.separator"));
        builder.append("Has photo: ");
        builder.append(getPhotoId() != null);
        builder.append(System.getProperty("line.separator"));
        builder.append("Is approved: ");
        builder.append(isApproved());

        return builder.toString();
    }

    public void updateValues(Payment updatedPayment) {
        user = updatedPayment.getUser();
        mCreator = updatedPayment.getmCreator();
        currency = updatedPayment.getCurrency();
        receiver = updatedPayment.getReceiver();
        amount = updatedPayment.getAmount();
        totalAmount = updatedPayment.getTotalAmount();
        //mOriginalAmount = updatedPayment.getOriginalAmount();
        description = updatedPayment.getDescription();
        //mTime = updatedPayment.getTime();
        photoId = updatedPayment.getPhotoId();
    }
}
