package com.example.commonpurse.customClasses.payments;

import java.util.ArrayList;

public class ScheduledPayment {

    private String firebaseId;
    private String name;
    private String creatorName;
    private String creatorId;
    private String databaseName;
    private double amount;
    private int intervalFrequency;
    private int intervalSize;
    private long dateCreated;
    private long lastRun;
    private boolean isActive;
    private ArrayList<Payment> payments;

    public String getFirebaseId() {
        return firebaseId;
    }

    public void setFirebaseId(String firebaseId) {
        this.firebaseId = firebaseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public int getIntervalFrequency() {
        return intervalFrequency;
    }

    public void setIntervalFrequency(int intervalFrequency) {
        this.intervalFrequency = intervalFrequency;
    }

    public int getIntervalSize() {
        return intervalSize;
    }

    public void setIntervalSize(int intervalSize) {
        this.intervalSize = intervalSize;
    }

    public long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public long getLastRun() {
        return lastRun;
    }

    public void setLastRun(long lastRun) {
        this.lastRun = lastRun;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public ArrayList<Payment> getPayments() {
        return payments;
    }

    public void setPayments(ArrayList<Payment> payments) {
        this.payments = payments;
    }
}
