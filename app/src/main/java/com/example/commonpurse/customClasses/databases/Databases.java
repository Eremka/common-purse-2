package com.example.commonpurse.customClasses.databases;


import androidx.databinding.BaseObservable;

public class Databases extends BaseObservable {

    private String key;
    private long timeCreated;
    private String name;
    private String password;
    private String currency;
    private int nrOfUsers;
    private String creator;

    public Databases() {
    }

    public void setTimeCreated(long timeCreated) {
        this.timeCreated = timeCreated;
    }

    public long getTimeCreated() {
        return timeCreated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) /*throws Exception*/ {
        if (name.length() > 3){
            this.name = name;
        } /*else throw new Exception();*/
    }

    public void setPassword(String password) /* throws Exception */{
        if (password.length() > 3) {
            this.password = password;
        } /*else throw new Exception();*/
    }

    public String getPassword(){
        return password;
    }

    public String getKey(){
        return key;
    }

    public void setKey (String key){
        this.key = key;
    }

    public String getCurrency() {return this.currency; }

    public void setCurrency(String currency){
        this.currency = currency;
    }

    public void setNrOfUsers(int nrOfUsers){
        this.nrOfUsers = nrOfUsers;
    }

    public int getNrOfUsers(){
        return this.nrOfUsers;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public void updateValues(Databases db){
        this.key = db.getKey();
        this.creator = db.getCreator();
        this.currency = db.getCurrency();
        this.name = db.getName();
        this.nrOfUsers = db.getNrOfUsers();
        this.password = db.getPassword();
        this.timeCreated = db.getTimeCreated();
    }
}
