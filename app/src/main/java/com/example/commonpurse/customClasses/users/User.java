package com.example.commonpurse.customClasses.users;

import com.example.commonpurse.helpers.stringFormatters.EmailAddressValidator;
import com.example.commonpurse.helpers.stringFormatters.TwoDecimalFormatter;

import java.math.BigDecimal;

public class User {

    public User() {
    }

    private String firebaseKey;
    private String firstName;
    private String lastName;
    private String name;
    private String id;
    private String emailAddress;
    private String role;
    private long timeCreated;
    private boolean isActive;
    private BigDecimal amount;
    private String photoUri;

    public String getFirebaseKey() {
        return this.firebaseKey;
    }

    public void setFirebaseKey(String firebaseKey) {
        this.firebaseKey = firebaseKey;
    }

    public String getPhotoUri() {
        return photoUri;
    }

    public void setPhotoUri(String photoUri) {
        this.photoUri = photoUri;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setFirstName(String firstName) {
        if (firstName == null || firstName.length() == 0) {
            throw new IllegalArgumentException("You must enter a first name");
        }
        this.firstName = firstName;
    }

    public void setEmailAddress(String emailAddress) {
        if (!EmailAddressValidator.checkEmailAddress(emailAddress)) {
            throw new IllegalArgumentException("This is not a valid email address");
        }

        this.emailAddress = emailAddress;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setTimeCreated(long timeCreated) {
        this.timeCreated = timeCreated;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }


    public String getId() {
        return id;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getRole() {
        return role;
    }

    public long getTimeCreated() {
        return timeCreated;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public BigDecimal getUserAmount() {
        return this.amount;
    }

    public void setUserAmount(double amount) {
        this.amount = TwoDecimalFormatter.formatAmount(new BigDecimal(amount));
    }

    /*
    public void setUserAmount(BigDecimal amount){
        this.amount = amount;
    }
    */

    public void setName(String name) {
        this.name = name;
    }

    public void updateValues(User user) {
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        //this.name = user.getName();
        this.id = user.getId();
        this.emailAddress = user.getEmailAddress();
        this.role = user.getRole();
        this.isActive = user.getIsActive();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Name: ").append(this.name)
                .append(System.getProperty("line.separator")).append("Email: ").append(this.emailAddress)
                .append(System.getProperty("line.separator")).append("Time created: ").append(this.timeCreated)
                .append(System.getProperty("line.separator")).append("Is inactive: ").append(this.isActive)
                .append(System.getProperty("line.separator")).append("Role: ").append(this.role)
                .append(System.getProperty("line.separator")).append("Firebase Key: ").append(getFirebaseKey());
        return sb.toString();
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
