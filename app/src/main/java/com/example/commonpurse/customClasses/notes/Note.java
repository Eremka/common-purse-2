package com.example.commonpurse.customClasses.notes;

import java.io.Serializable;

/**
 * Created by robbe on 20-2-2018.
 */

public class Note implements Serializable {

    public Note(){}

    private String id;
    private String title;
    private String user;
    private double amount;
    private String currency;
    private long timeCreated;
    private long timeAmended;
    private String bodyText;

    public Note(String title, String user, double amount, String currency, long timeCreated, long timeAmended, String bodyText) {
        this.title = title;
        this.user = user;
        this.amount = amount;
        this.currency = currency;
        this.timeCreated = timeCreated;
        this.timeAmended = timeAmended;
        this.bodyText = bodyText;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public long getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(long timeCreated) {
        this.timeCreated = timeCreated;
    }

    public long getTimeAmended() {
        return timeAmended;
    }

    public void setTimeAmended(long timeAmended) {
        this.timeAmended = timeAmended;
    }

    public String getBodyText() {
        return bodyText;
    }

    public void setBodyText(String bodyText) {
        this.bodyText = bodyText;
    }

    @Override
    public String toString() {
        return "Note{" +
                "title='" + title + '\'' +
                ", user='" + user + '\'' +
                ", amount=" + amount +
                ", currency='" + currency + '\'' +
                ", timeCreated=" + timeCreated +
                ", timeAmended=" + timeAmended +
                ", bodyText='" + bodyText + '\'' +
                '}';
    }

    public void updateValues(Note note){
        this.title = note.getTitle();
        this.user = note.getUser();
        this.amount = note.getAmount();
        this.currency = note.getCurrency();
        this.timeCreated = note.getTimeCreated();
        this.timeAmended = note.getTimeAmended();
        this.bodyText = note.bodyText;
    }
}
