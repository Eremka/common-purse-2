package com.example.commonpurse.helpers.constants;

import java.util.Calendar;

public class Constants {

    public static final int PERMISSION_WRITE_STORAGE = 10;

    public static final int DAYS = Calendar.DAY_OF_YEAR;
    public static final int WEEKS = Calendar.WEEK_OF_YEAR;
    public static final int MONTHS = Calendar.MONTH;

    public static final String DAYS_STRING = "Day(s)";
    public static final String WEEKS_STRING = "Week(s)";
    public static final String MONTHS_STRING = "Month(s)";

    public static final int NUMBER_PICKER_MIN_VALUE = 1;
    public static final int NUMBER_PICKER_MAX_VALUE = 31;

    public static final String SCHEDULE_DIALOG = "scheduleDialog";

    public static String getIntervalSizeString(int size) {
        switch (size) {
            case DAYS:
                return DAYS_STRING;
            case WEEKS:
                return WEEKS_STRING;
            case MONTHS:
                return MONTHS_STRING;
            default:
                return null;
        }
    }
}
