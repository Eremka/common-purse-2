package com.example.commonpurse.helpers.pictureResizer;

import android.graphics.Bitmap;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class PhotoResizer {

    private static final String TAG = PhotoResizer.class.getSimpleName();

    public static Bitmap resizePhoto(Bitmap b) throws IOException {
        try {
            Log.d(TAG, "resizePhoto bitmap: " + b.getByteCount());
            // original measurements
            int origWidth = b.getWidth();
            int origHeight = b.getHeight();

            final int destWidth = 400;//or the width you need

            if (origWidth > destWidth) {
                // picture is wider than we want it, we calculate its target height
                int destHeight = origHeight / (origWidth / destWidth);
                // we create an scaled bitmap so it reduces the image, not just trim it
                Bitmap b2 = Bitmap.createScaledBitmap(b, destWidth, destHeight, false);
                ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                // compress to the format you want, JPEG, PNG...
                // 70 is the 0-100 quality percentage
                b2.compress(Bitmap.CompressFormat.JPEG, 70, outStream);
                Log.d(TAG, "resizePhoto new bitmap size: " + b2.getByteCount());

                return b2;
            } else {
                return null;
            }
        } catch (Exception e){
            Log.d(TAG, "resizePhoto error message: " + e.getMessage());
            return null;
        }
    }
}
