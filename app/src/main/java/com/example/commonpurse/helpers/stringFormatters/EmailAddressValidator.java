package com.example.commonpurse.helpers.stringFormatters;

import android.util.Patterns;

import java.util.regex.Pattern;

public class EmailAddressValidator {

    public static boolean checkEmailAddress(String email){
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }
}
