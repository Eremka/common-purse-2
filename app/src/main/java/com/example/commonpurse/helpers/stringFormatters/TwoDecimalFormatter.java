package com.example.commonpurse.helpers.stringFormatters;

import android.util.Log;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Currency;

public class TwoDecimalFormatter {

    private static final String TAG = TwoDecimalFormatter.class.getSimpleName();
    
    public static String formatAmount(String currency, double amount){
        NumberFormat formatter = NumberFormat.getCurrencyInstance();

        if (currency != null) {
            Currency currencyInstance = Currency.getInstance(currency.toLowerCase());
            formatter.setCurrency(currencyInstance);
        }

        return formatter.format(amount);
    }

    public static String formatAmount(double amount){
        NumberFormat format = NumberFormat.getInstance();
        format.setMaximumFractionDigits(2);
        format.setMinimumFractionDigits(2);

        String res = format.format(amount);
        Log.d(TAG, "formatAmount res amount: " + res);

        return res.replaceAll(",", "");
    }

    public static BigDecimal formatAmount(BigDecimal amount){
        return amount.setScale(2, BigDecimal.ROUND_HALF_UP);
    }
}
