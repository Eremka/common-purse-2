package com.example.commonpurse.helpers.calculations;

import com.example.commonpurse.helpers.stringFormatters.UserIdFormatter;
import com.example.commonpurse.customClasses.payments.Payment;
import com.example.commonpurse.customClasses.users.User;

import java.math.BigDecimal;
import java.util.ArrayList;

public class UserCalculations {

    public static ArrayList<User> createUserListWithAmount(BigDecimal paymentAmount, ArrayList<User> userList,
                                        ArrayList<User> removedUserList, ArrayList<Payment> originalPaymentList) {
        ArrayList<User> returnList = new ArrayList<>(userList);
        returnList.removeAll(removedUserList);

        for (User u: returnList){
            if (originalPaymentList == null) {
                u.setUserAmount(paymentAmount.divide(new BigDecimal(returnList.size()), 2,
                        BigDecimal.ROUND_HALF_UP).doubleValue());
            } else {
                for (Payment p: originalPaymentList){
                    if (UserIdFormatter.getUserId(u).contains(p.getReceiver())){
                        if (p.getExchangeRate() == 0) {
                            u.setUserAmount(p.getAmount());
                        } else {
                            BigDecimal amount = new BigDecimal(p.getAmount());
                            BigDecimal exchangeRate = new BigDecimal(p.getExchangeRate());
                            u.setUserAmount(amount.divide(exchangeRate, 2,
                                    BigDecimal.ROUND_HALF_UP).floatValue());
                        }
                    }
                }
            }
        }

        return returnList;
    }
}
