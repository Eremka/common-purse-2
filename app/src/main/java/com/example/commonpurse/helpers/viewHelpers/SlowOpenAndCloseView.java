package com.example.commonpurse.helpers.viewHelpers;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;

public class SlowOpenAndCloseView {

    public static void openViewGradually(View view, View openView, int viewHeight) {
        openView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, viewHeight));
        openView.setVisibility(View.VISIBLE);
        TranslateAnimation animation = new TranslateAnimation(
                0,0,-(viewHeight),0
        );
        animation.setDuration(1000);
        view.startAnimation(animation);
    }

    public static void closeViewGradually(View view, View closeView, int viewHeight){

        TranslateAnimation animation = new TranslateAnimation(
                0,0,0,-(viewHeight)
        );
        animation.setDuration(1000);
        animation.setFillEnabled(true);
        animation.setFillBefore(true);
        animation.setFillAfter(true);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                closeView.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        view.startAnimation(animation);
    }

}
