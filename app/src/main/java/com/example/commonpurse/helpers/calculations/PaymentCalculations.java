package com.example.commonpurse.helpers.calculations;

import android.util.Log;

import com.example.commonpurse.helpers.stringFormatters.TwoDecimalFormatter;
import com.example.commonpurse.customClasses.users.User;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Random;

public class PaymentCalculations {

    private static final String TAG = PaymentCalculations.class.getSimpleName();

    public static ArrayList<User> calculatePaymentsBeforeSave(ArrayList<User> userList, BigDecimal totalAmount, BigDecimal exchangeRate){
        // Setting the exchange rate to 1 if there is no currency conversion. In this way you can
        // always multiply the amount times the exchange rate.
        exchangeRate = exchangeRate.equals(BigDecimal.ZERO) ? BigDecimal.ONE : exchangeRate;
        // Calcluating whether the amounts from the user list are matching the total amount.
        BigDecimal usersAmount = BigDecimal.ZERO;
        BigDecimal totalCalculatedAmount = totalAmount.multiply(exchangeRate);

        float margin = (float)userList.size() / 100;
        for (User u: userList){
            // Making sure that the amounts are rounded of to 2 decimals.
            // It also calculates the payment to the base payment currency amount.
            u.setUserAmount(u.getUserAmount().multiply(exchangeRate).floatValue());
            usersAmount = usersAmount.add(u.getUserAmount());
        }

        BigDecimal difference = TwoDecimalFormatter.formatAmount(usersAmount.subtract(totalCalculatedAmount));
        difference = difference.abs();
        if (difference.equals(TwoDecimalFormatter.formatAmount(new BigDecimal(0.00)))){
            Log.d(TAG, "calculatePaymentsBeforeSave equal: " + difference);
            return userList;
        } if (margin >= difference.floatValue() || margin * -1 >= difference.floatValue()){
            Log.d(TAG, "calculatePaymentsBeforeSave margin is bigger: " + (margin >= difference.floatValue()));
            Log.d(TAG, "calculatePaymentsBeforeSave margin times minus 1 is bigger: " + (margin * -1 >= difference.floatValue()));
            Log.d(TAG, "calculatePaymentsBeforeSave margin: " + margin);
            Log.d(TAG, "calculatePaymentsBeforeSave small difference: " + difference);
            return calculateExtraPaymentAmounts(userList, difference.floatValue());
        } else {
            return null;
        }
    }

    private static ArrayList<User> calculateExtraPaymentAmounts(ArrayList<User> userList, float dif){
        // Adding 0.01 cent to random users in the list.
        if (dif > 0.00f){
            int difference = Math.round(dif * 100);
            int random = new Random().nextInt(userList.size() - 1);
            for (int i = random; i < (random + difference); i++) {
                User unluckyUser;
                if (i >= userList.size()) {
                    unluckyUser = userList.get(i - userList.size());
                } else {
                    unluckyUser = userList.get(i);
                }
                float newAmount = unluckyUser.getUserAmount().floatValue();
                unluckyUser.setUserAmount(newAmount -= 0.01f);
            }
        } else {
            int difference = Math.round(dif * 100 * -1);
            int random = new Random().nextInt(userList.size() - 1);
            for (int i = random; i < (random + difference); i++) {
                User unluckyUser;
                if (i >= userList.size()) {
                    unluckyUser = userList.get(i - userList.size());
                } else {
                    unluckyUser = userList.get(i);
                }
                BigDecimal newAmount = unluckyUser.getUserAmount();
                unluckyUser.setUserAmount(newAmount.add(new BigDecimal(0.01)).doubleValue());
            }
        }

        return userList;
    }
}
