package com.example.commonpurse.helpers.calculations;

import com.example.commonpurse.helpers.stringFormatters.UserIdFormatter;
import com.example.commonpurse.customClasses.payments.Payment;
import com.example.commonpurse.customClasses.users.User;

import java.math.BigDecimal;
import java.util.ArrayList;

public class PaymentListCreator {

    public static ArrayList<Payment> createPaymentList(User paymentCreator, BigDecimal totalAmount, String currency, String description,
                                                BigDecimal exchangeRate, long time, ArrayList<User> userList){
        ArrayList<Payment> paymentArrayList = new ArrayList<>();
        boolean basePayment = true;

        for (User u: userList){
            Payment payment = new Payment();
            payment.setAmount(u.getUserAmount().floatValue());
            payment.setCurrency(currency);
            payment.setExchangeRate(exchangeRate.doubleValue());
            payment.setApproved(true);
            payment.setDescription(description);
            payment.setmTime(time);
            payment.setReceiver(UserIdFormatter.getUserId(u));
            payment.setUser(UserIdFormatter.getUserId(paymentCreator));

            // If the exchange rate is not 0, the payment was done in a foreign currency. Thus the
            // payment amounts need to be calculated according to the exchange rate.
            if (exchangeRate.equals(BigDecimal.ZERO) || exchangeRate.equals(BigDecimal.ONE)){
                payment.setTotalAmount(totalAmount.floatValue());
            } else {
                payment.setmOriginalAmount(totalAmount.floatValue());
                payment.setTotalAmount(totalAmount.multiply(exchangeRate).floatValue());
            }
            payment.setmCreator(UserIdFormatter.getUserId(paymentCreator));

            // Setting the basePayment. There can only be one base payment hence the base payment
            // after it has been set, is set to false.
            payment.setBasePayment(basePayment);
            basePayment = false;
            paymentArrayList.add(payment);
        }

        return paymentArrayList;
    }
}
