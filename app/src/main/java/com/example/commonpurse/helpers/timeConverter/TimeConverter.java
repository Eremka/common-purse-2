package com.example.commonpurse.helpers.timeConverter;

import java.text.DateFormat;
import java.util.Date;

public class TimeConverter {

    private static final String TAG = TimeConverter.class.getSimpleName();

    public static String getDateString(long time) {
        Date date = new Date(time);
        return DateFormat.getDateInstance().format(date).trim();
    }
}
