package com.example.commonpurse.helpers.stringFormatters;

import androidx.annotation.NonNull;

import com.example.commonpurse.customClasses.users.User;

public class UserIdFormatter {
    public static String getUserId(@NonNull User user){
        String[] ids = user.getId().split(",");
        return ids[0];
    }
}
