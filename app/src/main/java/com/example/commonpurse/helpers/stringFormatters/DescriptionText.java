package com.example.commonpurse.helpers.stringFormatters;

import com.example.commonpurse.helpers.timeConverter.TimeConverter;

public class DescriptionText {

    public static String getDescriptionText(String name, long time){
        StringBuilder sb = new StringBuilder();
        sb.append("Created by ");
        sb.append(name);
        sb.append(" on ");
        sb.append(TimeConverter.getDateString(time));

        return sb.toString();
    }

    public static String getExchangeRateText(String baseCurrency, String selectedCurrency, float rate){
        StringBuilder sb = new StringBuilder();
        sb.append("The current exchange rate is: 1 ");
        sb.append(selectedCurrency);
        sb.append(" equals ");
        sb.append(rate);
        sb.append(" ");
        sb.append(baseCurrency);

        return sb.toString();
    }
}
