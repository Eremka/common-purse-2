package com.example.commonpurse.helpers.databaseHelper;

import com.google.firebase.database.FirebaseDatabase;

public class FirebaseDatabaseHelper {

    private static FirebaseDatabase firebaseDatabase;

    public static FirebaseDatabase getFirebaseDatabaseInstance(){
        if (firebaseDatabase == null){
            firebaseDatabase = FirebaseDatabase.getInstance();
            firebaseDatabase.setPersistenceEnabled(true);
        }

        return firebaseDatabase;
    }

}
